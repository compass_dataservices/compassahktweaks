#SingleInstance Force

#Include <JSON>
#Include <NativeZip2>

;Now = 20160113181358
;last_date = 2016-01-13
;last_time = 16:15:55.127367 (GMT)

; Example: Download text to a variable:
CheckForUpdate(last_update_time) {
	try{
		whr := ComObjCreate("WinHttp.WinHttpRequest.5.1")
		whr.Open("GET", "https://bitbucket.org/api/2.0/repositories/compass_dataservices/compassahktweaks/downloads", true)
		whr.Send()
		; Using 'true' above and the call below allows the script to remain responsive.
		whr.WaitForResponse()
	} catch e {
		TrayTip, CompassAHKTweaks, Unable to check for new version
		return
	}
	
	source_json := whr.ResponseText

	parsed := JSON.Load(source_json)

	parsed_out := Format("
	(Join`r`n
	Last Download: {}
	Download Link: {}
	)"
	, parsed.values[1].created_on, parsed.values[1].links.self.href)

	update_time_string := StrReplace(parsed.values[1].created_on, "-","")
	update_time_string := StrReplace(update_time_string, ":","")
	update_time_string := StrReplace(update_time_string, ".","")
	update_time_string := StrReplace(update_time_string, "+","")
	update_time_string := StrReplace(update_time_string, "T","")
	update_time_string := SubStr(update_time_string, 1, -10)

	;Convert GMT to local
	update_time_string := update_time_string - 50000
	if (update_time_string > last_update_time) {
		MsgBox, 4, CompassAHKTweaks Update, New update available.  Would you like to download?
		IfMsgBox, Yes
		{
			download_link := parsed.values[1].links.self.href
			download_name := parsed.values[1].name
			DownloadUpdate(download_link, download_name)
		}
	} else {
		TrayTip, CompassAHKTweaks, Running latest version
	}
}

DownloadUpdate(download_link, download_name) {
	UrlDownloadToFile, %download_link%, %download_name%
	msgBox, 4, CompassAHKTweaks Update, % "Download success!  Would you like to install it now?"
	IfMsgBox, Yes
	{
		IniWrite, %A_Now%, CompassTweaks.ini, Updates, Last Updated
		ExtractAndInstall(download_name)
	}
}

ExtractAndInstall(file_name)
{
	;Extract
	Unz(A_WorkingDir . "\" . file_name, A_WorkingDir . "\temp")
	
	;Install
	RunWait, %A_WorkingDir%\temp\CompassAHKTweaks_Setup.exe
}