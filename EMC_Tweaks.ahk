;;;;;;;;;;;;;;;;;;;;
;	EMC - Micros   ;
;;;;;;;;;;;;;;;;;;;;

EMC_F4_OVERRIDE := False

#IfWinActive ahk_group EMCGroup

:*:7M::
	Send 700000
return

~LButton::
	If (EMCTweaksEnabled){
		OverrideBypassCheck = 0
		RaisePageCheck = 0
		If EMCOverrideBypassEnabled
			SetTimer, OverrideBypass, 10
		SetTimer, RaisePageDesignError, 10
	}
return

KillWindowTimers:
	SetTimer, OverrideBypass, Off
	SetTimer, RaisePageDesignError, Off
	SetTimer, BarcodeSaveAnnoyanceBypass, Off
return
	
OverrideBypass:
	If (OverrideBypassCheck < 300) {
		IfWinExist, Override Record
		{
			ControlSend, Yes, {Space}, Override Record
			GoSub, KillWindowTimers
		}
	} else {
		GoSub, KillWindowTimers
	}
return

RaisePageDesignError:
	If (RaisePageCheck < 300) {
		IfWinExist,,You cannot edit Pages
		{
			WinActivate,, You cannot edit Pages
			GoSub, KillWindowTimers
		}
	} else {
		GoSub, KillWindowTimers
	}
return

BarcodeSaveAnnoyanceBypass:
	If (SaveAnnoyanceCheck < 300) {
		IfWinExist,,Your changes have been saved.
		{
			ControlSend, OK, {Space},,Your changes have been saved.
			GoSub, KillWindowTimers
		}
	} else {
		GoSub, KillWindowTimers
	}
return

;testing Numpad Ins
NumpadIns::
	If (EMCTweaksEnabled && DebuggingEnabled) {
		Send {Ins}
		SetNumLockState, On
	}
return

^r::
Categorizer:
	If (EMCTweaksEnabled) {
		ControlGetFocus, c
		if (c = "WindowsForms10.EDIT.app.0.378734a9") {
			Send {TAB 7}
		} else {
			Send {End}
			Sleep, 100
			Send +{Home}
		}
		Sleep, 100
		Send %MAJOR_GROUP%
		Sleep, 100
		Send {Tab}
		Sleep, 100
		Send %FAMILY_GROUP%
		Sleep, 100
		Send {Tab}
		Send ^s
		Sleep, 300
		Send {Down}{Left 2}
	}
return

;unchecker
^F2::
	breakUncheker := false
	if (EMCTweaksEnabled) {
		InputBox, loopCount, "Loop Count", "How many boxes?"
		Loop, %loopCount% 
		{ 
			Send {Space}
			Send {Down}
			if  breakUncheker
				 break
		} 
	} 
return
	
;Easier Additions to Combo Groups
=::
^=::
	if (EMCTweaksEnabled) {
		ControlFocus, %EMC_COMBO_GROUP_ADD_ITEM%
		Send {Enter}
		Sleep, 300
		ControlFocus, %EMC_COMBO_GROUP_SELECT%
		Sleep, 300
		Send {Enter}
	}
return

;Current Control Info
^!g::
	if (DebuggingEnabled && EMCTweaksEnabled) {
		ControlGetFocus, ControlName
		if ErrorLevel
		MsgBox, The target window doesn't exist or none of its controls has input focus.
		else {
			MsgBox, Control with focus = %ControlName%
			clipboard=%ControlName%
		}
	}
return

;Goto with uberClipboard
^+g::
	if (EMCTweaksEnabled) {
		Send ^g
		Sleep, 300
		SendLevel, 2
		Send ^1
		Send {End}+{Home}
		Send ^+{Ins}
		SendLevel, 0
		Sleep, 300
		ControlFocus, %EMC_PRICE_EDIT%
		ControlFocus, %EMC29_PRICE_EDIT%
	}
return

^!c::
GrabMenuItemDetails:
	fullExtract := true
!c::
GrabMenuItemNumber:
	Keywait, Alt
	If (EMCTweaksEnabled) {
		Clipboard=
		Sleep, 300
		Send ^c
		ClipWait, 1
		global xmlDoc
		xmlDoc := loadXML(Clipboard)
		Sleep, 300
		if (xmlDoc.parseError.errorCode == 0) {
			if (fullExtract == true)
				Clipboard := getButtonText(extractButtonText(xmlDoc, -1))
			else
				Clipboard := getMenuID(extractButton(xmlDoc, -1))
			numLines = 0
			Loop, Parse, Clipboard, `n, `r
				numLines := A_Index - 1
			If (numLines > 1) 
				sendToUberClipboard()
			TrayTip, CompassAHKTweaks, Menu ID Copied
		} else {
			TrayTip, CompassAHKTweaks, Unable to extract Menu ID. Please Try again.
		}
	}
	fullExtract := false
return

^+c::
	If (EMCTweaksEnabled) {
		GoSub, GrabMenuItemNumber
	} else {
		GoSub, uberClipCopy
	}
return

^+PgUp::
EMCUberPastePlus:
	if (EMCTweaksEnabled && DebuggingEnabled) {
		TrayTip, DEBUG, EMC uberClipPaste
		ControlGetFocus, c
		If (c = EMC_MASTER_EDIT_UNFOCUSED) {
			Send {F2}
			Sleep, 300
		}
	}
return

;enhanced barcode paste with uberClipboard
$!+Ins::
EMCUberBarcodeSearch:
	tabName := GetEMCTab()
	subTabName := GetEMCSubTab()
	IfInString, tabName, Barcode 
	{
		SendLevel, 5
		Send ^f
		Sleep, 3000
		SendLevel, 2
		Send ^+{Ins}
	}
	SendLevel, 0
return

Esc::
	if (EMCTweaksEnabled) {
		IfWinActive, Insert Price Record
		{
			ControlFocus, Cancel
			Send {Space}
		} else {
			Send {Esc}
		}
	} else {
		Send {Esc}
	}
return

~^s::
	SetTimer, BarcodeSaveAnnoyanceBypass, -750
return	

;Auto-Save on enter press in several places
;Move focus to OK button when pressing Enter in Add Menu Items window

$Enter::
NumpadEnter::
	if (EMCTweaksEnabled) {
		IfWinActive, Add Menu Items
		{
			ControlGetFocus, ControlName
			if (ControlName = EMC_ADD_MENU_ITEMS_OK_BUTTON or ControlName = EMC29_ADD_MENU_ITEMS_OK_BUTTON) {
				Send {SPACE}
				Sleep, 1000
				IfWinNotActive ahk_exe EMC.exe
				{
					WinActivate ahk_exe EMC.exe
				}
			} else if (ControlName = EMC_FILTER_BARCODES_EDIT or ControlName = EMC29_FILTER_BARCODES_EDIT) {
				ControlFocus, %EMC_INSERT_PRICES_OK_BUTTON%
				ControlFocus, %EMC29_INSERT_PRICES_OK_BUTTON%
				Send {SPACE}
			} else {
				ControlFocus, %EMC_ADD_MENU_ITEMS_OK_BUTTON%
				ControlFocus, %EMC29_ADD_MENU_ITEMS_OK_BUTTON%
			}
			return
		}
		IfWinActive, Insert Price Record
		{
			;Automatically select first price sequence
			;Current implementation is buggy and prone to timing or reversing previously enabled items
			ControlGetFocus, ControlName
			if (ControlName = EMC_INSERT_PRICES_DEFAULT_EDIT or ControlName = EMC29_INSERT_PRICES_DEFAULT_EDIT) {
				ControlFocus, %EMC_INSERT_PRICES_LISTBOX%
				ControlFocus, %EMC29_INSERT_PRICES_LISTBOX%
				Send {Space} ;selects first price sequence
				Sleep, 100
				Send, {Space} ;checks box
			}
			
			ControlFocus, %EMC_INSERT_PRICES_OK_BUTTON%
			ControlFocus, %EMC29_INSERT_PRICES_OK_BUTTON%
			Send {Space}
			return
		}
		IfWinActive, Insert Record
		{
			ControlFocus, OK
			Send {Space}
			return
		}
		IfWinActive, Select Menu Item Master
		{
			;Generate Legend on Page Design
			
			ControlFocus, OK
			Send {Space}
			WinWaitNotActive, Select Menu Item Master
			Sleep, 100
			Send +{Tab 2}
			Send {Enter}
			return
		}
		IfWinActive, Add Employees
		{
			ControlGetFocus, c
			if (c = EMC_ADD_EMP_OK_BUTTON or c = EMC29_ADD_EMP_OK_BUTTON) {
				Send {Space}
			} else {
				ControlFocus, %EMC_ADD_EMP_OK_BUTTON%
				ControlFocus, %EMC29_ADD_EMP_OK_BUTTON%
			}
		}
		IfWinActive, Select Master
		{
			ControlFocus, OK
			Send {Space}
			return
		}
		ControlGetFocus, ControlNameX
		if (ControlNameX = EMC_FILTER_BARCODES_EDIT or ControlNameX = "WindowsForms10.Window.8.app.0.378734a3"
			or ControlNameX = EMC_ITEM_PRICE_EDIT or ControlNameX = EMC_PRICE_EDIT
			or ControlNameX = EMC29_FILTER_BARCODES_EDIT or ControlNameX = EMC29_ITEM_PRICE_EDIT
			or ControlNameX = EMC29_PRICE_EDIT)
		{
			Send {ENTER}
			Send ^s
			SaveAnnoyanceCheck = 0
			SetTimer, BarcodeSaveAnnoyanceBypass, 10
		} else if (ControlNameX = "WindowsForms10.EDIT.app.0.378734a2") {
			Send {TAB}
			Send {SPACE}
		} else {
			Send {ENTER}
		}
	} else {
		Send {ENTER}
	}
return

^Enter::
	if (EMCTweaksEnabled && DebuggingEnabled) {
		ControlGetFocus, ControlName
		if (ControlName = EMC_ADD_MENU_ITEMS_OK_BUTTON) {
			Send {SPACE}
			GoSub,itemIterator
		}
	}
return

;auto-save when pasting with F4
~F4::
	if (EMCTweaksEnabled && EMC_F4_OVERRIDE) {
		ControlGetFocus, ControlNameX
		if (ControlNameX = "WindowsForms10.RichEdit20W.app.0.378734a1" or ControlNameX = "WindowsForms10.Window.8.app.0.378734a3" or ControlNameX = "WindowsForms10.Window.8.app.0.378734a16")
		{
			Send ^s
		} else {
			MsgBox, 4, EMC Tweaks, Are you sure you want to exit?
			IfMsgBox Yes
				WinClose, ahk_class WindowsForms10.Window.8.app.0.378734a, 0
			else
				return
		}
	}
return

!e::
EMCPageButtonEdit:
	if (EMCTweaksEnabled) {
		tabName := GetEMCTab()
		IfInString, tabName, Page Design
		{
			ControlFocus, %EMC_PAGE_DESIGN_ANCHOR%
			Send {Tab 8}
			Send {Enter}
		}
	}
return

^\::
	if (DebuggingEnabled) {
		msgBox Nothing to see here.
	}
return

;Ctrl-F for quick search
$^f::
EMCFastFilter:
	if (EMCTweaksEnabled) {
		ControlFocus, %EMC_FILTER_RESULTS_COMBOBOX%
		if (errorlevel) {
			ControlFocus, %EMC29_FILTER_RESULTS_COMBOBOX%
		}
		
		tabName := GetEMCTab()
		subTabName := GetEMCSubTab()
		IfInString, tabName, Barcode 
		{
			Sleep, 100
			ControlFocus, Clear Filters
			Send {Enter}
			ControlSend, %EMC29_CLEAR_FILTERS%, {Enter}, EMC
			Sleep, 2000
			Control, ChooseString, Barcode,% EMC_FIRST_COMBOBOX, ahk_exe EMC.EXE
			if (errorlevel) {
				Control, ChooseString, Barcode,% EMC29_FIRST_COMBOBOX, ahk_exe EMC.EXE
			}
			ControlFocus, %EMC_FILTER_BARCODES_EDIT%, ahk_exe EMC.EXE
			if (errorlevel) {
				ControlFocus, %EMC29_FILTER_BARCODES_EDIT%, ahk_exe EMC.EXE
			}
			return
		} else {
		IfInString, tabName, Menu Item Maintenance
		{
			IfInString, subTabName, Master
			{
				Control, ChooseString, Name,% EMC_FILTER_RESULTS_COMBOBOX
				if (errorlevel) {
					Control, ChooseString, Name,% EMC29_FILTER_RESULTS_COMBOBOX
				}
			}
			IfInString, subTabName, Definition
			{
				;attempt to select first name for *Definition Records
				Control, ChooseString, First,% EMC_FILTER_RESULTS_COMBOBOX
				if (errorlevel) {
					Control, ChooseString, First,% EMC29_FILTER_RESULTS_COMBOBOX
				}
			}
			IfInString, subTabName, Price
			{
				;Definition Records
				Control, ChooseString, Definition Name,% EMC_FILTER_RESULTS_COMBOBOX
				if (errorlevel) {
					Control, ChooseString, Definition Name,% EMC29_FILTER_RESULTS_COMBOBOX
				}
			}
		}
		}
		ControlFocus, %EMC_FILTER_RESULTS_EDIT%
		if (errorlevel) {
			ControlFocus, %EMC29_FILTER_RESULTS_EDIT%
		}
	}
return

;built-in find re-map
!f::
	if (EMCTweaksEnabled) {
		Send {Esc}
		Send ^f
	}
return

;save re-map
!s::
Keywait, Alt
	if (EMCTweaksEnabled) {
		Send ^s
		Send {Esc}
		Send ^s
	}
return

;intercept paste keyboard shortcut and trim leading zeroes when necessary
^v::
	if (EMCTweaksEnabled) {
		ControlGetFocus, cN
		if (cN = "WindowsForms10.EDIT.app.0.33c0d9d1" or cN = EMC_ADD_MENU_ITEMS_ALL_PRICE_EDIT
		or cN = EMC29_ADD_MENU_ITEMS_ALL_PRICE_EDIT)
		{
			trimBarcode()
		} else {
			Send ^v
		}
	} else {
		Send ^v
	}
return

+Enter::
	if (EMCTweaksEnabled) {
		ControlGetFocus, ControlName
		if (ControlName = EMC_FILTER_RESULTS_EDIT or ControlName = EMC_FILTER_BARCODES_EDIT
		or ControlName = EMC29_FILTER_RESULTS_EDIT or ControlName = EMC29_FILTER_BARCODES_EDIT)
		{
			Send {TAB 5}
			Send {ENTER}
			Send +{TAB 5}
		}
	}
return

;allows back button on mouse to close current tabName
XButton1::
    if (EMCTweaksEnabled)
		MsgBox, 4, CompassAHKTweaks, Are you sure you want to close this tab?
		IfMsgBox, Yes
		{
			Send ^q
		}
return

TAB::
	if (EMCTweaksEnabled) {
		IfWinActive, Add Menu Items
		{
			ControlGetFocus, ControlName
			if (ControlName = EMC_FIRST_COMBOBOX or ControlName = EMC29_FIRST_COMBOBOX) {
				ControlFocus, %EMC_ADD_MENU_ITEMS_NAME_EDIT%
				ControlFocus, %EMC29_ADD_MENU_ITEMS_NAME_EDIT%
			} else if (ControlName = EMC_ADD_MENU_ITEMS_IND_PRICE_RADIO or ControlName = EMC29_ADD_MENU_ITEMS_IND_PRICE_RADIO) {
				ControlFocus, %EMC_ADD_MENU_ITEMS_PRICE_EDIT%
				ControlFocus, %EMC29_ADD_MENU_ITEMS_PRICE_EDIT%
				Send {DOWN}
			} else {
				Send {TAB}
			}
		} else IfWinActive, Add Employees 
		{
			ControlGetFocus, c
			if (c = EMC_FIRST_COMBOBOX or c = EMC29_FIRST_COMBOBOX) {
				ControlFocus, %EMC_ADD_EMP_FNAME_EDIT%
				ControlFocus, %EMC29_ADD_EMP_FNAME_EDIT%
			} else if (c = EMC_ADD_EMP_RECNUM_EDIT or c = EMC29_ADD_EMP_RECNUM_EDIT) {
				ControlFocus, %EMC_ADD_EMP_OK_BUTTON%
				ControlFocus, %EMC29_ADD_EMP_OK_BUTTON%
			} else {
				Send {TAB}
			}
		} else IfWinActive, Select Menu Item Master 
		{
			ControlGetFocus, c
			if (c = EMC_SELECT_MENU_MASTER_OBJNUMBER_EDIT or c = EMC29_SELECT_MENU_MASTER_OBJNUMBER_EDIT)
				ControlFocus, %EMC_SELECT_MENU_MASTER_NAME_EDIT%
				ControlFocus, %EMC29_SELECT_MENU_MASTER_NAME_EDIT%
			if (c = EMC_SELECT_MENU_MASTER_NAME_EDIT or c = EMC29_SELECT_MENU_MASTER_NAME_EDIT)
				ControlFocus, %EMC_SELECT_MENU_MASTER_LIST_BOX%
				ControlFocus, %EMC29_SELECT_MENU_MASTER_LIST_BOX%
		} else {
			Send {TAB}
		}
	} else {
		Send {TAB}
	}
return

^!t::
	if (DebuggingEnabled) {
		thisTab := GetEMCTab()
		TrayTip, CompassAHKTweaks, % "Current EMC Tab: " . thisTab
	}
return

GetEMCTab()
{
	WinGetText, results, EMC
	result := ""
	Loop, Parse, results, `n
	{
		if (A_Index == 1) {
			result := A_LoopField
		}
		
	}
	return result
}

GetEMCSubTab()
{
	global EMC_27_CLASS, EMC_29_CLASS
	WinGetText, results, EMC
	WinGetClass, winClass, A
	if (winClass = EMC_27_CLASS) {
		subTabIndex := 4
	} else if (winClass = EMC_29_CLASS) {
		subTabIndex := 31
	}
	result := ""
	Loop, Parse, results, `n
	{
		if (A_Index = subTabIndex) {
			result := A_LoopField
			break
		}
	}
	return result
}

itemIterator:
	Loop
	{
		if (A_Index > 10) {
			TrayTip, CompassAHKTweaks, Aborting loop to prevent infinity
			break
		}
		Clipboard=
		WinWaitNotActive, Add Menu Items,,2
		IfWinExist,, The selected menu item number already exists
		{
			ControlSend, OK, {Space},, The selected menu item number already exists
			ControlFocus, WindowsForms10.EDIT.app.0.378734a2, Add Menu Items
			Send {HOME}
			Send +{END}
			Send ^c
			ClipWait,1
			itemNumber := Clipboard
			itemNumber++
			Sleep 100
			Clipboard=
			Clipboard := itemNumber
			ClipWait,1
			Send ^v
			Sleep 100
			ControlFocus, OK, Add Menu Items
			If ErrorLevel
				ControlFocus, OK, Add Menu Items
			Sleep 100
			Send {SPACE}
		} else {
			IfWinExist, Add Menu Item Master
			{	
				WinWait, Item Added Successfully
				WinActivate, Item Added Sucessfully
				break
			} else {
				msgBox No results window located. Aborting.`nLast Item ID attempted:%itemNumber%
				break
			}
		}
	}
return