;;;;;;;;;;;;;;;;;;;;
;	CONSTANTS      ;
;;;;;;;;;;;;;;;;;;;;


PROG_VERSION = 1.1
PROG_TAG = Compass DS Tools

;EMC
MAJOR_GROUP = 0
FAMILY_GROUP = 0
EMC_27_CLASS = WindowsForms10.Window.8.app.0.378734a
EMC_29_CLASS = WindowsForms10.Window.8.app.0.13965fa_r11_ad1

;EMC Controls
EMC_ADD_MENU_ITEMS_OK_BUTTON = WindowsForms10.BUTTON.app.0.378734a5
EMC29_ADD_MENU_ITEMS_OK_BUTTON = WindowsForms10.BUTTON.app.0.13965fa_r11_ad15
EMC_ADD_MENU_ITEMS_NAME_EDIT = WindowsForms10.EDIT.app.0.378734a4
EMC29_ADD_MENU_ITEMS_NAME_EDIT = WindowsForms10.EDIT.app.0.13965fa_r11_ad14
EMC_ADD_MENU_ITEMS_IND_PRICE_RADIO = WindowsForms10.BUTTON.app.0.378734a3
EMC29_ADD_MENU_ITEMs_IND_PRICE_RADIO = WindowsForms10.BUTTON.app.0.13965fa_r11_ad13
EMC_ADD_MENU_ITEMS_ALL_PRICE_RADIO = WindowsForms10.BUTTON.app.0.378734a4
EMC29_ADD_MENU_ITEMS_ALL_PRICE_RADIO = WindowsForms10.BUTTON.app.0.13965fa_r11_ad14
EMC_ADD_MENU_ITEMS_ALL_PRICE_EDIT = WindowsForms10.EDIT.app.0.378734a1
EMC29_ADD_MENU_ITEMS_ALL_PRICE_EDIT = WindowsForms10.EDIT.app.0.13965fa_r11_ad112 ;unverified
EMC_ADD_MENU_ITEMS_RADIO_BUTTON = WindowsForms10.BUTTON.app.0.378734a2
EMC29_ADD_MENU_ITEMS_RADIO_BUTTON = WindowsForms10.BUTTON.app.0.13965fa_r11_ad12
EMC_ADD_MENU_ITEMS_PRICE_EDIT = WindowsForms10.Window.8.app.0.378734a3
EMC29_ADD_MENU_ITEMS_PRICE_EDIT = WindowsForms10.Window.8.app.0.13965fa_r11_ad13
EMC_FILTER_RESULTS_EDIT = WindowsForms10.EDIT.app.0.378734a9
EMC29_FILTER_RESULTS_EDIT = WindowsForms10.EDIT.app.0.13965fa_r11_ad111
EMC_FILTER_BARCODES_EDIT = WindowsForms10.EDIT.app.0.378734a1
EMC29_FILTER_BARCODES_EDIT = WindowsForms10.EDIT.app.0.13965fa_r11_ad112
EMC_FILTER_RESULTS_COMBOBOX = WindowsForms10.COMBOBOX.app.0.378734a3
EMC29_FILTER_RESULTS_COMBOBOX = WindowsForms10.COMBOBOX.app.0.13965fa_r11_ad13
EMC_FIRST_COMBOBOX = WindowsForms10.COMBOBOX.app.0.378734a1
EMC29_FIRST_COMBOBOX = WindowsForms10.COMBOBOX.app.0.13965fa_r11_ad15
EMC_INSERT_PRICES_LISTBOX = WindowsForms10.LISTBOX.app.0.378734a1
EMC29_INSERT_PRICES_LISTBOX = WindowsForms10.LISTBOX.app.0.13965fa_r11_ad11
EMC_INSERT_PRICES_DEFAULT_EDIT = WindowsForms10.EDIT.app.0.378734a2
EMC29_INSERT_PRICES_DEFAULT_EDIT = WindowsForms10.EDIT.app.0.13965fa_r11_ad12
EMC_INSERT_PRICES_OK_BUTTON = WindowsForms10.BUTTON.app.0.378734a2
EMC29_INSERT_PRICES_OK_BUTTON = WindowsForms10.BUTTON.app.0.13965fa_r11_ad12 ;unverified
EMC_RESULTS_NAME_COLUMN = WindowsForms10.Window.8.app.0.378734a15
EMC29_RESULTS_NAME_COLUMN = ACKETZ
EMC_ADD_EMP_FNAME_EDIT = WindowsForms10.EDIT.app.0.378734a6
EMC29_ADD_EMP_FNAME_EDIT = ACKETZ
EMC_ADD_EMP_RECNUM_EDIT = WindowsForms10.EDIT.app.0.378734a1
EMC29_ADD_EMP_RECNUM_EDIT = WindowsForms10.EDIT.app.0.13965fa_r11_ad112 ;unverified
EMC_ADD_EMP_OK_BUTTON = WindowsForms10.BUTTON.app.0.378734a2
EMC29_ADD_EMP_OK_BUTTON = ACKETZ
EMC_BARCODES_LIST = WindowsForms10.Window.8.app.0.378734a3
EMC29_BARCODES_LIST = WindowsForms10.Window.8.app.0.13965fa_r11_ad13 ;unverified
EMC_BARCODES_MASTER_EDIT = WindowsForms10.RichEdit20W.app.0.378734a2
EMC29_BARCODES_MASTER_EDIT = WindowsForms10.EDIT.app.0.13965fa_r11_ad142
EMC_ITEM_PRICE_EDIT = WindowsForms10.RichEdit20W.app.0.378734a1
EMC29_ITEM_PRICE_EDIT = WindowsForms10.EDIT.app.0.13965fa_r11_ad12
EMC_COMBO_GROUP_ADD_ITEM = WindowsForms10.STATIC.app.0.378734a27
EMC29_COMBO_GROUP_ADD_ITEM = ACKETZ
EMC_COMBO_GROUP_EDIT_PRIMARY = WindowsForms10.EDIT.app.0.378734a8
EMC29_COMBO_GROUP_EDIT_PRIMARY = ACKETZ
EMC_COMBO_GROUP_SELECT = WindowsForms10.STATIC.app.0.378734a25
EMC29_COMBO_GROUP_SELECT = ACKETZ
EMC_SELECT_MENU_MASTER_OBJNUMBER_EDIT = WindowsForms10.EDIT.app.0.378734a1
EMC29_SELECT_MENU_MASTER_OBJNUMBER_EDIT = WindowsForms10.EDIT.app.0.13965fa_r11_ad112 ;unverified
EMC_SELECT_MENU_MASTER_NAME_EDIT = WindowsForms10.EDIT.app.0.378734a2
EMC29_SELECT_MENU_MASTER_NAME_EDIT = WindowsForms10.EDIT.app.0.13965fa_r11_ad12 ;unverified
EMC_SELECT_MENU_MASTER_LIST_BOX = WindowsForms10.Window.8.app.0.378734a2
EMC29_SELECT_MENU_MASTER_LIST_BOX = 
EMC_PAGE_DESIGN_ANCHOR = WindowsForms10.Window.8.app.0.378734a5
EMC29_PAGE_DESIGN_ANCHOR = 
EMC_BATCH_EDIT_CHECKNUM_EDIT = WindowsForms10.EDIT.app.0.378734a4
EMC29_BATCH_EDIT_CHECKNUM_EDIT = ACKETZ
EMC_BATCH_EDIT_LISTVIEW = WindowsForms10.EDIT.app.0.378734a2
EMC29_BATCH_EDIT_LISTVIEW = WindowsForms10.EDIT.app.0.13965fa_r11_ad12 ;unverified
EMC_BATCH_EDIT_LISTVIEWX = WindowsForms10.Window.8.app.0.378734a3
EMC29_BATCH_EDIT_LISTVIEWX = WindowsForms10.Window.8.app.0.13965fa_r11_ad13 ;unverified
EMC_BATCH_EDIT_SYSTREE = WindowsForms10.SysTreeView32.app.0.378734a1
EMC29_BATCH_EDIT_SYSTREE = ACKETZ
EMC_EDIT_BATCH_RECORD_OMIT_CHECKBOX = WindowsForms10.BUTTON.app.0.378734a4
EMC29_EDIT_BATCH_RECORD_OMIT_CHECKBOX = WindowsForms10.BUTTON.app.0.13965fa_r11_ad14 ;unverified
EMC_BATCH_EDIT_BATCH_SELECT = WindowsForms10.EDIT.app.0.378734a2
EMC29_BATCH_EDIT_BATCH_SELECT = WindowsForms10.EDIT.app.0.13965fa_r11_ad12 ;unverified
EMC_MASTER_EDIT_UNFOCUSED = WindowsForms10.Window.8.app.0.378734a15
EMC29_MASTER_EDIT_UNFOCUSED = 
EMC_MASTER_EDIT_FOCUSED = WindowsForms10.RichEdit20W.app.0.378734a1
EMC29_MASTER_EDIT_FOCUSED = WindowsForms10.EDIT.app.0.13965fa_r11_ad12 ;unverified
EMC_PRICE_EDIT = WindowsForms10.RichEdit20W.app.0.378734a2
EMC29_PRICE_EDIT = WindowsForms10.EDIT.app.0.13965fa_r11_ad142 ;unverified
EMC29_CLEAR_FILTERS = WindowsForms10.STATIC.app.0.13965fa_r11_ad1172

;UD Controls
UD_ITEM_TABS = WindowsForms10.Window.8.app.0.33c0d9d6
UD_ITEM_PRICELEVEL_EDIT1 = WindowsForms10.EDIT.app.0.33c0d9d10
UD_ADVANCED_SEARCH_KEYWORD_EDIT = WindowsForms10.EDIT.app.0.33c0d9d1
UD_ADVANCED_SEARCH_CONTAINS_RADIO = WindowsForms10.BUTTON.app.0.33c0d9d8
UD_ADVANCED_SEARCH_SKU_CHECKBOX = WindowsForms10.BUTTON.app.0.33c0d9d13
UD_ADVANCED_SEARCH_BEGINS_RADIO = WindowsForms10.BUTTON.app.0.33c0d9d7
UD_ITEM_SEARCH_NAME_EDIT = WindowsForms10.EDIT.app.0.33c0d9d9
UD_REPORT_SETUP_DATE_START = Edit2
UD_REPORT_SETUP_DATE_END = Edit3
UD_ADVANCED_SEARCH_IDNUM_EDIT = WindowsForms10.Window.8.app.0.33c0d9d8
UD_ITEM_ID_TOP_EDIT = WindowsForms10.Window.8.app.0.33c0d9d90
UD_SKU_FIELD = WindowsForms10.EDIT.app.0.33c0d9d10
UD_SKU_FIELD2 = WindowsForms10.EDIT.app.0.33c0d9d12

;RegEX Patterns
Price = \$?\d+\.\d{2}
Barcode = (?<=(\s|#))[\d\s-]{5,13}(?=(\s|$))
BUTTON_XPATH := "//*[local-name()='Button']"
TEXT_XPATH := "//*[local-name()='TranslatedEntry']"

