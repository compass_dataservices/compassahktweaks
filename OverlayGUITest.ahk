#SingleInstance Force

Gui +LastFound +AlwaysOnTop +ToolWindow

fieldOne := ["Snickers", "Milky Way", "3 Musketeers"]
fieldTwo := ["012345678912", "01234568", "812345678955"]
fieldThree := ["1.25", "0.99", "0.99"]

maxPos := fieldOne.length()
rec := 1

xPos := 0
yPos := A_ScreenHeight - 153
CustomColor = EEAA99
WinSet, TransColor, %CustomColor% 200
	
;Gui, UberOverlay:Margin, 0, 0                         
Gui, Add, Edit, w200 xm+2 ReadOnly vField1, % fieldOne[1]
Gui, Add, Edit, wp x+m ReadOnly vField2, % fieldTwo[1]
Gui, Add, Edit, wp x+m ReadOnly vField3, % fieldThree[1]
Gui, Add, Button, w60 h20 xm gCopyField, Copy &1
Gui, Add, Button, wp x225 yp gCopyField, Copy &2
Gui, Add, Button, wp x432 yp gCopyField, Copy &3
Gui, Add, UpDown, w100 xm+250 vRec gChangeRecord Range1-%maxPos% Horz, 1
Gui, Show, X%xPos% Y%yPos% NoActivate
return
;Gui, UberOverlay:Font, S12, Verdana

ChangeRecord:
	GuiControl,, Field1,% fieldOne[rec]
	GuiControl,, Field2,% fieldTwo[rec]
	GuiControl,, Field3,% fieldThree[rec]
return

#IfWinActive

^1::
	CopyField(1)
return

^2::
	CopyField(2)
return

^3::
	CopyField(3)
return

CopyField:
	StringRight, eVar, A_GuiControl, 1
	CopyField(eVar)
return

CopyField(fieldNum)
{
	global rec
	global fieldOne
	global fieldTwo
	global fieldThree
	if (fieldNum = 1)
	{
		Clipboard := fieldOne[rec]
	}
	else if (fieldNum = 2)
	{
		Clipboard := fieldTwo[rec]
	}
	else if (fieldNum = 3)
	{
		Clipboard := fieldThree[rec]
	}
	Clipwait, 1
}

	
GuiEscape:
TrayTip, Uber Overlay, Exiting
Sleep, 1000
GuiClose:
ExitApp
