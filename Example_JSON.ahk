#SingleInstance Force

#Include <JSON>

;Now = 20160113181358
;last_date = 2016-01-13
;last_time = 16:15:55.127367 (GMT)

json_str =
(
{
	"str": "Hello World",
	"num": 12345,
	"float": 123.5,
	"true": true,
	"false": false,
	"null": null,
	"array": [
		"Auto",
		"Hot",
		"key"
	],
	"object": {
		"A": "Auto",
		"H": "Hot",
		"K": "key"
	}
}
)

;FileRead, source_json, Source.json

; Example: Download text to a variable:
whr := ComObjCreate("WinHttp.WinHttpRequest.5.1")
whr.Open("GET", "https://bitbucket.org/api/2.0/repositories/compass_dataservices/compassahktweaks/downloads", true)
whr.Send()
; Using 'true' above and the call below allows the script to remain responsive.
whr.WaitForResponse()
source_json := whr.ResponseText

parsed := JSON.Load(source_json)

; parsed_out := Format("
; (Join`r`n
; String: {}
; Number: {}
; Float:  {}
; true:   {}
; false:  {}
; null:   {}
; array:  [{}, {}, {}]
; object: {{}A:""{}"", H:""{}"", K:""{}""{}}
; )"
; , parsed.str, parsed.num, parsed.float, parsed.true, parsed.false, parsed.null
; , parsed.array[1], parsed.array[2], parsed.array[3]
; , parsed.object.A, parsed.object.H, parsed.object.K)

parsed_out := Format("
(Join`r`n
Last Download: {}
Download Link: {}
)"
, parsed.values[1].created_on, parsed.values[1].links.self.href)

last_updated := parsed.values[1].created_on
last_updated := StrSplit(last_updated, "T")
update_date := last_updated[1]
update_date := StrSplit(update_date, "-")
last_time := last_updated[2]
update_time := StrSplit(last_time, "+")[1]
update_time_string := StrReplace(parsed.values[1].created_on, "-","")
update_time_string := StrReplace(update_time_string, ":","")
update_time_string := StrReplace(update_time_string, ".","")
update_time_string := StrReplace(update_time_string, "+","")
update_time_string := StrReplace(update_time_string, "T","")
update_time_string := SubStr(update_time_string, 1, -10)

;Convert GMT to local
update_time_string := update_time_string - 50000
Clipboard := update_time_string
Clipwait, 1
;update_time := last_time[1]
download_link := parsed.values[1].links.self.href
download_name := parsed.values[1].name

;UrlDownloadToFile, %download_link%, %download_name%

msgBox % "Download success!`nDate: " update_date "`nTime: " update_time "`nString: " update_time_string

stringified := JSON.Dump(parsed,, 4)
stringified := StrReplace(stringified, "`n", "`r`n") ; for display purposes only

ListVars
WinWaitActive ahk_class AutoHotkey
ControlSetText Edit1, %A_Now%`r`n[PARSED]`r`n%parsed_out%`r`n`r`n[STRINGIFIED]`r`n%stringified%
WinWaitClose
return