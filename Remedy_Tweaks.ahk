;;;;;;;;;;;;;;
;   Remedy   ;
;;;;;;;;;;;;;;
#UseHook

#IfWinActive ahk_group RemedyGroup


;Grab Current Control Name
^!g::
	if (DebuggingEnabled) {
		ControlGetFocus, ControlName
		if ErrorLevel
			MsgBox, The target window doesn't exist or none of its controls has input focus.
		else {
			MsgBox, Control with focus = %ControlName%
			clipboard=%ControlName%
		}
	}
return

HOME::
	StatusBarGetText, remUsername, 3
	if (remUsername = "millea05") {
		Send {HOME}
	}
	else {
		Send !{HOME}
	}
return

^HOME::
	StatusBarGetText, remUsername, 3
	if (remUsername = "millea05") {
		Send ^{HOME}
	}
	else {
		Send {HOME}
	}
return


;alternate save hotkey
!s::
^s:: 
	WinGetTitle, Title, A
	if (Title = "BMC Remedy User - [FSS Task (Modify)]") {
		Clipboard=
		GoSub, ResetTabStops
		Send {Tab 10}!m{ESC}
		Send ^c
		ClipWait, 1
		ticketStatus := Clipboard
		if (ticketStatus = "Completed") {
			Clipboard=
			GoSub, ResetTabStops
			Send {Tab 3}!m{ESC}
			Send ^c
			ClipWait, 1
			if (Clipboard = "New Request") {
				msgBox % "The ticket type must be set before saving."
				return
			}
		}
	}
	Send ^{ENTER}
return


^F4::
	Send ^w
return

XButton1::
^w::
CloseWindow:
	Send ^{F4}
return

#IfWinActive BMC Remedy User - [FSS:Console (Search)]
MButton::
F5::
	ControlFocus, SysTabControl321
	Send {Tab 14}
	Send {Enter}
return

^+o::
	TrayTip, CompassAHKTweaks, Rickety Tickety Tock
	Send {Enter}
	Sleep 3000
	IFWinExist, BMC Remedy User - [FSS Task (Modify)]
	{
		TrayTip, CompassAHKTweaks, Attempting Ticket Scrape
		WinActivate, BMC Remedy User - [FSS Task (Modify)]
		GoSub,CopyTicketInfo
	} else {
		TrayTip, CompassAHKTweaks, No eligible window to scrape from.
	}
return

;Inserts current date into Go Live Date if tab focus is on Unit Number text box
#IfWinActive BMC Remedy User - [FSS:Request
^d::
	GoSub, SetGoLiveDate
return

#IfWinActive BMC Remedy User - [FSS:Request (New)]
^+d::
	GoSub, SetGoLiveDate
	; sets Request is For: to Support (RFS)
	Send {Tab}
	Send !m
	Send s
	; sets Assigned Group: to FSS Data Services Team
	Send {Tab 4}
	Send !m
	Send {Down}{Enter}
	; sets Solutions Offered: to Data Services
	Send {Tab 6}
	Send {Down 4}{Enter}
	; sets focus to the save button
	Send {Tab}
return	
	

#IfWinActive BMC Remedy User - [FSS Task (Modify)]

CoordMode, Mouse, Relative  ;sets coordinates based upon active window

;shortcut to view link for modifying ticket properties

^+v::
	Gosub, ResetTabStops
	Send {Tab 9}{Enter}
	Sleep, 1000
	Send +{End}
return
	
^1::
OpenAttachment:
	BlockInput, On
	Sleep, 300
	ControlFocus, SysTabControl321, BMC
	If ErrorLevel
		ControlFocus, SysTabControl321, BMC
	Send ^+{Home}
	Send {Right}
	Send {Tab 16}
	AttachmentArray := Object()
	Loop, 10
	{
		BlockInput, On
		Send {Down}
		Clipboard=
		Send ^c
		Clipwait, 1
		attachment := StrSplit(Clipboard, A_Tab)
		aName := attachment[1]
		if (!RegExMatch(aName, ".*xlsx?$") and !RegExMatch(aName, ".*docx?$") and !RegExMatch(aName, ".*pptx?$"))
			continue
		if (AttachmentArray.MaxIndex() > 0) and (aName in AttachmentArray) {
			break
		} else {
			AttachmentArray.insert(aName)
			Send {Enter}
			Sleep, 500
			Send {Tab}
			Send {Enter}
			
		}
		BlockInput, Off
		WinWaitClose, Attachment
	}
	BlockInput, Off
	TrayTip, CompassAHKTweaks, Finished Opening Attachments
return

;copy ticket info
^+o::
CopyTicketInfo:
	KeyWait, Ctrl
	KeyWait, Shift
    ControlGetFocus, ControlName
    if (ControlName = "Internet Explorer_Server1" or ControlName = "Internet Explorer_Server2") {
        msgBox % "Wrong control in focus, cannot scrape ticket info."
        return
    }
	Gosub,ScrapeTicket
	Sleep 500
	titleMatchStore := A_TitleMatchMode
	SetTitleMatchMode, 2
	IfWinExist, OneNote
	{
		TrayTip, CompassAHKTweaks, Pasting to OneNote
		WinActivate, OneNote
		WinWaitActive,,,5
		If (ErrorLevel) {
			msgBox % "Unable to Activate OneNote"
			return
		}
		Send ^n
		Sleep 1000
		IfWinActive, OneNote
		{
			GoSub,PasteTicketToOneNote
		} else {
			msgBox % "Aborting OneNote paste"
		}
	} else {
		msgBox % "Unable to locate OneNote Window"
	}
	WinActivate, Remedy
	SetTitleMatchMode, %titleMatchStore%
	TrayTip, CompassAHKTweaks, OneNote Paste Operation Complete.
	Sleep, 2000
	GoSub,OpenAttachment
return

;marking ticket as complete
^+f::
	saveClip := ClipboardAll
	Clipboard=
	GoSub, ResetTabStops
	Sleep 100
	Send {Tab 10}
	Sleep 100
	Send !m
	Send {Esc}
	Send ^c
	ClipWait, 1
	if (Clipboard != "Completed") {
		Send !m
		Send {c 2}
		Send {Enter}
	} else {
		msgBox Ticket is already marked as completed.
		return
	}
	Sleep 100
	ControlFocus, SysTabControl321
	Send ^+{Home}
	Send {Right}
	Clipboard=
	Sleep 100
	Send {Tab 15}
	Send {Home}
	Send +{End}
	Send ^c
	ClipWait, 1
	if (Clipboard = "Completed")
		return
	else
		Send Completed
	GoSub,IncreaseTicketCount
	Clipboard := saveClip
return

;marking ticket as complete
^+c::
	if (DebuggingEnabled) {
		saveClip := ClipboardAll
		Clipboard=
		GoSub, ResetTabStops
		Sleep 100
		Send {Tab 10}
		Sleep 100
		Send !m
		Send {Esc}
		Send ^c
		ClipWait, 1
		if (Clipboard != "Cancelled") {
			Send !m
			Send c
			Send {Enter}
		} else {
			msgBox Ticket is already marked as cancelled.
		}
		Sleep 100
		Clipboard := saveClip
	}
return

;set ticket type to InfoGenesis
^i::
	setTicketType("iG", "adhoc")
return

;set ticket type to iG Report
^+i::
	setTicketType("iG", "report")
return

;set ticket type to Simphony
^e::
	setTicketType("simp", "adhoc")
return

^+e::
	setTicketType("simp", "report")
Return

!e::
	GoSub,OpenEmail
return

;Assign tickets
^r::
	assignTicket("r")
return

^2::
	assignTicket("a")
return

^j::
	assignTicket("j")
return

^h::
	assignTicket("h")
return

;open new email, Tasks Info must be the tab currently open
OpenEmail:
	StatusBarGetText, remUsername, 3
	firstName = ""
	
	if(remUsername = "millea05" or remUsername = "beamar01") {
		saveClip := ClipboardAll
		Clipboard =
		ControlFocus, SysTabControl321
		Send ^+{Home}
		Send {Tab 3}
		Send +{Home}
		Send ^c
		ClipWait, 1
		RegExMatch(Clipboard,"([^\s]+)", firstName)
	}	
		ControlFocus, SysTabControl321
		Send ^+{Home}
		Send {right 3}
		Send {Tab 2}
		Send {Enter}
		WinWait, ahk_class AfxFrameOrView70,,3
		Send {Tab}
		
	if(firstName != "")
		Send Hi %firstName%,{Enter 2}This ticket has been completed.{Enter 2}Thanks,{Enter}FSS Data Services{TAB 3}
	Clipboard := saveClip	
return

;WindowText test method
^!w::
	currentTab := GetRemedyTab()
	msgBox, Current Tab is %currentTab%
return

GetRemedyTab()
{
	WinGetText, RemedyWindowText, BMC Remedy User - [FSS Task (Modify)]
	StringGetPos, InfoTextPos, RemedyWindowText, Tasks Info
	StringGetPos, ActivityTextPos, RemedyWindowText, Activity
	StringGetPos, HistoryTextPos, RemedyWindowText, Task History
	StringGetPos, EmailTextPos, RemedyWindowText, Email
	StringGetPos, SharepointTextPos, RemedyWindowText, Sharepoint
	StringGetPos, UnitActivityTextPos, RemedyWindowText, Unit Activity
	textPosArray := {Info: InfoTextPos, Activity: ActivityTextPos, TaskHistory: HistoryTextPos, Email: EmailTextPos, Sharepoint: SharepointTextPos, UnitActivity: UnitActivityTextPos}
	activeTab := ""
	lowestPos = 99999
	For key, value in textPosArray
	{
		if (value > 0 && value < lowestPos) {
			lowestPos := value
			activeTab := key
		}
	}
	return %activeTab%
}

;Ensures the tab focus is at the first tab stop, the Summary field
ResetTabStops:
	ControlGetFocus, ControlName
	if (ControlName = "SysTabControl321" or ControlName = "AfxWnd706") {
		Send {Tab}
		Sleep, 100
	}
	Send ^+{Home}
return

;Scrapes relevant info from pre-existing ticket
ScrapeTicket:
	GoSub,ClearTicketVars
	saveClip := ClipboardAll
    if (GetRemedyTab() = "Info") {
		Clipboard = 
		Sleep 200
		GoSub, ResetTabStops
		Send {Tab 5}
		Send +{Home}
		Send ^c
		ClipWait, 1
		If (ErrorLevel) {
			TrayTip, CompassAHKTweaks, Ticket Scrape failed prematurely at taskID.
			Sleep 1000
			return
		}
		taskID := Clipboard
		Clipboard =
		Sleep 200
		Send {Tab 8}
		Send +{Home}
		Send ^c
		ClipWait, 1
		If (ErrorLevel) {
			TrayTip, CompassAHKTweaks, Ticket Scrape failed prematurely at unitNum.
			Sleep 1000
			return
		}
		unitNum := Clipboard
		Clipboard =
		Sleep 200
		Send {Tab 2}
		Send +{Home}
		Send ^c
		ClipWait, 1
		If (ErrorLevel) {
			TrayTip, CompassAHKTweaks, Ticket Scrape failed prematurely at requesterName.
			Sleep 1000
			requesterName := "No Name"
		} else {
			requesterName := Clipboard
		}
		Clipboard =
		Sleep 200
		Send {Tab 2}
		Send +{Home}
		Send ^c
		ClipWait, 1
		If (ErrorLevel) {
			TrayTip, CompassAHKTweaks, Ticket Scrape failed prematurely at requesterPhone.
			Sleep 1000
			requesterPhone := "No Phone"
		} else {
			requesterPhone := Clipboard
		}
		Clipboard =
		Sleep 200
		Send {Tab}
		Send +{Home}
		Send ^c
		ClipWait, 1
		If (ErrorLevel) {
			TrayTip, CompassAHKTweaks, Ticket Scrape failed prematurely at requesterEmail.
			Sleep 1000
			requesterEmail := "No Email"
		} else {
			requesterEmail := Clipboard
		}
		Sleep 100
		Clipboard=
		Sleep 200
		Send {Tab 19}
		Send ^{Home}
		Send ^+{Down 500}
		Send ^c
		ClipWait, 10
		If ErrorLevel
			noTicketContents := True
		else
			noTicketContents := False
		ticketContents := Clipboard
	} else {
		ControlFocus, SysTabControl321
		Sleep 100
		Send ^+{Home}
		Sleep 100
		GoSub,%A_ThisLabel%
	}
	If (noTicketContents)
		TrayTip, CompassAHKTweaks, Requester Info Scraped.
	else
		TrayTip, CompassAHKTweaks, Ticket Data Scraped
	Sleep 1000
	Clipboard := saveClip
	;MsgBox Ticket Number: %taskID%`nUnit Number: %unitNum%`nRequester: %requesterName%`nPhone Number: %requesterPhone%`nEmail: %requesterEmail%
return

ClearTicketVars:
	taskID = 
	unitNum = 
	requesterName = 
	requesterPhone = 
	requesterEmail =
return

assignTicket(x)
{
	ControlFocus, SysTabControl321
	Send ^+{Home}
	GoSub,ResetTabStops
	Send {TAB 26}
	Send !m
	if (x != "j" and x != "h") {
		Send %x%
	} else if (x = "j") {
		Send {%x% 3}{ENTER}
	} else if (x = "h") {
		Send {j 4}{ENTER}
	}
	return
}

setTicketType(tType, tItem)
{
	GoSub, ResetTabStops
	Send {Tab 3}
	Send !m
	if (tType = "iG") {
		Send i
	} else if (tType = "simp") {
		Send s
	}
	Send {Enter}
	Send !m
	if (tItem = "adhoc") {
		Send {Down}
	} else if (tItem = "report") {
		Send {Down 6}
	}
	Send {Enter}
	return
}

SetGoLiveDate:
	ControlFocus, SysTabControl321
	Send {Left 5}
	Send {Tab 21}
	FormatTime, Time,, MM/dd/yyyy
	Send %Time%
return