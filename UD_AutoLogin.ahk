;UD Auto-Login

secondTry := false
#IfWinActive Login ahk_group UDGroup

;manual unit addition
^#a::GoSub,GrabText

Hotkey, IfWinActive, Login ahk_group UDGroup
Hotkey, ~LButton,, Off

;Easier System Login
TAB::
	if (parseCompleted != 1) {
		ParseSystemMap()
		GoSub,BulletProofUDLaunch
		GoSub,Reloader
	}
	If (UDTimeoutExtensionEnabled) {
		SetTimer, PreventUDIdleClose, 60000
	}
	if (UDTweaksEnabled) {
		ControlGetFocus, ControlName
		if (ControlName = "WindowsForms10.EDIT.app.0.33c0d9d3" or ControlName = "WindowsForms10.EDIT.app.0.1e6fa8e4") {
			ControlGetText, user, WindowsForms10.EDIT.app.0.33c0d9d2, Login ahk_class WindowsForms10.Window.8.app.0.33c0d9d
			InputBox, UnitNumber, System Selection, Please enter a unit number:,,220,150,,,,,%lastUnitNumber%
			lastUnitNumber := UnitNumber
			if (errorlevel) {
				return
			} else {
				data := UnitSearch(UnitNumber)
				if (data = "acket") {
					msgBox, Unable to find unit number.
					Hotkey, IfWinActive, Login ahk_class WindowsForms10.Window.8.app.0.33c0d9d
					Hotkey, ~LButton, GrabWithMouse, On
					return
				}
				ExtractUnitInfo(data)
				if (!unitSystem)
					return
				errorlevel := 0
				Control, ChooseString, %unitSystem%, WindowsForms10.COMBOBOX.app.0.33c0d9d1, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
				if (errorlevel) {
					Loop, 50 
					{
						errorlevel := 0
						Sleep 100
						Control, ChooseString, %unitSystem%, WindowsForms10.COMBOBOX.app.0.33c0d9d5, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
						Sleep 200
						if (!errorlevel)
							break
					}
					if (errorlevel) {
						MsgBox, System select fail at: %unitSystem%.  Please try again.
						return
					}
				}
				if (CheckClientVersion() = "acketz") {
					GoSub, KillUDLogin
					return
				}
				ControlSend, Enter, {Enter}, Login ahk_class WindowsForms10.Window.8.app.0.33c0d9d
				if (unitCustomer = "x") {
					TrayTip, Compass Tweaks, Single Unit System..., 10, 1
					return
				} else {
					secondTry := false
					WinWait, Login ahk_class WindowsForms10.Window.8.app.0.33c0d9d,Authenticating, 5
					WinWaitClose, Login ahk_class WindowsForms10.Window.8.app.0.33c0d9d,Authenticating, 20
					Sleep, 500
					IfWinExist Authentication Failure
					{
						ControlSend, OK, {Enter}, Authentication Failure
						TrayTip, CompassAHKTweaks, You may need to check your password., 10, 1
						GoSub,ErrorUnableToSelectUnit
					} else {
						IfWinExist Question
						{
							return
						} else {
							WinWait, Login ahk_class WindowsForms10.Window.8.app.0.33c0d9d, Getting Login, 1
							WinWaitClose, Login ahk_class WindowsForms10.Window.8.app.0.33c0d9d, Getting Login, 30
							GoSub,SelectUnit
						}
					}
				}
				return
			}
		} else {
			Send {TAB}
		}
	} else {
		Send {TAB}
	}
return

;re-attempt unit selection
^r::GoSub,SelectUnit

Esc::GoSub,GoBack


GoBack:
	selectButton = "WindowsForms10.BUTTON.app.0.33c0d9d2"
	cancelButton = "WindowsForms10.BUTTON.app.0.33c0d9d1"
	ControlGetFocus, ControlNameX
	if (InStr(ControlNameX, "edit") || ControlNameX = selectButton || ControlNameX = cancelButton) {
		ControlSend, Cancel, {Space}, Login ahk_class WindowsForms10.Window.8.app.0.33c0d9d
		ControlFocus, WindowsForms10.EDIT.app.0.33c0d9d2, Login ahk_class WindowsForms10.Window.8.app.0.33c0d9d
	}
return

SelectUnit:
	Control, ChooseString, %unitCustomer%, WindowsForms10.COMBOBOX.app.0.33c0d9d4
	Control, ChooseString, %unitEnterprise%, WindowsForms10.COMBOBOX.app.0.33c0d9d3
	Control, ChooseString, %unitDivision%, WindowsForms10.COMBOBOX.app.0.33c0d9d2
	Control, ChooseString, %unitName%, WindowsForms10.COMBOBOX.app.0.33c0d9d1
	Sleep 500
	if (ErrorLevel && secondTry) {
		GoSub,ErrorUnableToSelectUnit
	} else if (ErrorLevel) {
		secondTry := true
		GoSub,%A_ThisLabel%
	} else {
		ControlSend, ahk_parent, {Enter}, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
		WinWait, Universal Desktop ahk_class WindowsForms10.Window.8.app.0.33c0d9d, Getting Login, 2
	}
return

ErrorUnableToSelectUnit:
	FormatTime, RightNow, %A_Now%, M/d/y h:mm:ss tt
	MsgBox, 262147, CompassAHKTweaks, Looks like you don't have access to %unitName%.`nWould you like to try again with a different login?
	AgilysysInfo =
		( LTrim
		Store: %unitName%
		System: %unitSystem%
		Enterprise: %unitEnterprise%
		Customer: %unitCustomer%
		Division: %unitDivision%
		)
	IfMsgBox, No
	{
		AgilysysPhone = Agilysys Phone Number: 1.800.327.7088 (Option 1, 4)
		MsgBox, 4160, CompassAHKTweaks, 
		(LTrim 
		You can call Agilysys to have this store added to your login.
		`n%AgilysysPhone%
		`n`n%AgilysysInfo%
		)
	} else {
		IfMsgBox, Yes
		{
			WinActivate, Login ahk_class WindowsForms10.Window.8.app.0.33c0d9d
			GoSub,GoBack
		}
	}
	IniWrite, %unitSystem%, %A_MyDocuments%\Agilysys_Unit_Corrections-%user%.ini, %UnitNumber%, System
	IniWrite, %unitName%, %A_MyDocuments%\Agilysys_Unit_Corrections-%user%.ini, %UnitNumber%, Store
	IniWrite, %unitEnterprise%, %A_MyDocuments%\Agilysys_Unit_Corrections-%user%.ini, %UnitNumber%, Enterprise
	IniWrite, %unitCustomer%, %A_MyDocuments%\Agilysys_Unit_Corrections-%user%.ini, %UnitNumber%, Customer
	IniWrite, %unitDivision%, %A_MyDocuments%\Agilysys_Unit_Corrections-%user%.ini, %UnitNumber%, Division
	IniWrite, %RightNow%, %A_MyDocuments%\Agilysys_Unit_Corrections-%user%.ini, %UnitNumber%, Last Access Attempt
	IniWrite, %RightNow%, %A_MyDocuments%\Agilysys_Unit_Corrections_By_System-%user%.ini, %unitSystem%, %unitNumber%
	TrayTip, CompassAHKTweaks, Writing Unit info to log...
Return

UnitSearch(mUnitNumber)
{
	file := A_WorkingDir . "\UD_DB.csv"
	
	IfNotExist, %file%
	{
		MsgBox, 4, Error, Unable to locate csv file. Would you like to try again?
		IfMsgBox, No
			return "acket"
		else
			%A_ThisFunc%(mUnitNumber)
	}
			
	Loop, Read, %file%
	{
		if (ErrorLevel) {
			msgBox, File Read Fail!
			break
		}
		mField := StrSplit(A_LoopReadLine, ",")
		If (mField[1] = mUnitNumber){
			return %A_LoopReadLine%
		}
	}
	return "acket"
}

UnitSearchDB(mUnitNumber, ByRef db)
{
	rs := db.OpenRecordSet("SELECT unit_number, customer, enterprise, division, name, pos, system FROM units WHERE unit_number=" mUnitNumber)
	if (rs.EOF) {
		return "acket"
	} else {
		result := rs["unit_number"] "," rs["name"] "," rs["system"] "," rs["customer"] "," rs["enterprise"] "," rs["division"] "," rs["pos"]
		return %result%
		rs.Close()
	}
	return "acket"
}

ExtractUnitInfo(readline)
{
	unitDetails := StrSplit(readline, ",")
	global unitNumber := unitDetails[1]
	global unitName := unitDetails[2]
	tempSystem := unitDetails[3]
	global unitCustomer := unitDetails[4]
	global unitEnterprise := unitDetails[5]
	global unitDivision := unitDetails[6]
	global unitPOS := unitDetails[7]
	
	IniRead, iniSystem, CompassTweaks.ini, Database System Map, %tempSystem%, 0
	if (iniSystem)
		global unitSystem = iniSystem
	else
		global unitSystem = tempSystem
	Return
}

NumpadEnter::
Enter::
	ControlGetFocus, cName
	if (cName = "Edit1" or cName = "WindowsForms10.BUTTON.app.0.33c0d9d2") {
		GoSub,GrabText
	}
	Send {Enter}
return

GrabWithMouse:
	MouseGetPos,,,,mControl
	if (mControl = "WindowsForms10.BUTTON.app.0.33c0d9d2") {
		GoSub,GrabText
		ControlSend, WindowsForms10.BUTTON.app.0.33c0d9d2, {Enter}, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
	}
return	

GrabText:
	;Get CustomerChoice Position
	ControlGetText, Customer, Edit4, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
	ControlGetText, Enterprise, Edit3, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
	ControlGetText, Division, Edit2, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
	ControlGetText, Store, Edit1, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
	
	hiddenTextStore := A_DetectHiddenText
	DetectHiddenText, On
	WinGetText, winText, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
	DetectHiddenText, %hiddenTextStore%
	FoundPos := RegExMatch(winText, "[Ss]ystem\d{1,2}", SystemVar)
	if (FoundPos = 0) {
		FileAppend, %winText%`n`n, WinText.txt
		InputBox, SystemInput, System Selection, Please enter system number:,,220,150,,,,,
		if (errorlevel) {
			return
		}
		RegExMatch(SystemInput, "\d{1,2}", SystemNumber)
		SystemVar := "System" . SystemNumber
	}
	Sleep 500
	;Unit,UnitName,System,Customer,Enterprise,Division
	AddUnit(Store, SystemVar, Customer, Enterprise, Division)
	Hotkey, IfWinActive, Login ahk_class WindowsForms10.Window.8.app.0.33c0d9d
	Hotkey, LButton,, Off
return

CheckClientVersion()
{
	WinGetText, t, Login
	v6 := RegExMatch(t, "4\.4\.5")
	v7 := RegExMatch(t, "4\.4\.6")
	if (v6 > 1 and v7 > 1) {
		if (v6 > v7) {
			MsgBox % "This unit is on a system that has already been upgraded.  Please use other UD."
			return "acketz"
		} else {
			MsgBox % "This unit is on a system that hasn't been upgraded yet.  Please use other UD."
			return "acketz"
		}
	} else {
		return
	}
}

ParseUnitNumber(unit)
{
	RegExMatch(unit, "\d{4,5}(?=\W)", result)
	if (result = "") {
		InputBox, result, Unit Number, Please enter a unit number for %unit%:,,220,150
		if ErrorLevel
			return "acketz"
	}
	return result
}

AddUnit(Store, System, Customer, Enterprise, Division, UnitNumber="new")
{
	UnitFound := false
	Loop, Read, %A_WorkingDir%\UD_DB.csv
	{
		if (ErrorLevel) {
			msgBox, File Read Fail!
			break
		}
		unitDetails := StrSplit(A_LoopReadLine, ",")
		{
			If (unitDetails[2] = Store){
				UnitFound := true
				break
			}
		}
		If UnitFound
			break
	}
	If !UnitFound {
		If (UnitNumber = "new") {
			UnitNumber := ParseUnitNumber(Store)
			If (UnitNumber = "acketz")
				return
		}
		FileAppend, %UnitNumber%`,%Store%`,%system%`,%Customer%`,%Enterprise%`,%Division%`,IG`n, UD_DB.csv
		TrayTip, CompassAHKTweaks, Adding new unit.
		Sleep 500
	} Else {
		TrayTip, CompassAHKTweaks, Unit already exists in database.
		Sleep 500
	}
Return
}