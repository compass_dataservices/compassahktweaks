; CompassAHKTweaks
#Persistent
#MaxMem 128
#ClipboardTimeout 5000

SendMode InputThenPlay

; #Include <DBA>

OnExit UnRegister ; unregister before exiting
DebuggingToggleEnabled := !A_IsCompiled
FormatTime, TimeString, %A_Now%, M/d/y h:mm:ss tt

GroupAdd, RemedyGroup, BMC Remedy
GroupAdd, RemedyGroup, FSS Send Mail

GroupAdd, UDGroup, ahk_class WindowsForms10.Window.8.app.0.1e6fa8e
GroupAdd, UDGroup, ahk_class WindowsForms10.Window.8.app.0.33c0d9d

GroupAdd, EMCGroup, ahk_class WindowsForms10.Window.8.app.0.378734a
GroupAdd, EMCGroup, ahk_class WindowsForms10.Window.8.app.0.13965fa_r11_ad1


;Menu Items
Menu, Tray, NoStandard
Menu, UDToggleMenu, Add, Enable UD Tweaks, UDFlagToggle
Menu, UDToggleMenu, Add, Enable UD Timeout Extension, UDTimeoutToggle
Menu, EMCToggleMenu, Add, Enable EMC Tweaks, EMCFlagToggle
Menu, EMCToggleMenu, Add, Enable EMC Override Bypass, EMCOverrideBypassToggle
Menu, ToggleMenu, Add, Enable Global Hotkeys, GlobalHKFlagToggle
Menu, ToggleMenu, Add, Enable Slack Hotkey, SlackFlagToggle
Menu, ToggleMenu, Add, Enable Global Barcode Tools, BarcodeToolsToggle
Menu, ToggleMenu, Add, UD, :UDToggleMenu
Menu, ToggleMenu, Add, EMC, :EMCToggleMenu
Menu, Tray, Add, Get Store Info, GetStoreInfo
Menu, Tray, Add, Activate Uber Clipboard, sendToUberClipboard
Menu, Tray, Add, Update Universal Desktop Login Database, ManualDBUpdate
Menu, HelpMenu, Add, Show Hotkey Shortcuts, ShowHotkeyShortcutGUI
Menu, HelpMenu, Add, Show Hotkey List, Display_HotkeyList
Menu, HelpMenu, Add, Reset Ticket Counter Location, ResetTicketCounterLocation
Menu, HelpMenu, Add, Manual UD System Parse, ParseSystemMap
Menu, HelpMenu, Add, Bug Reporting, Go_Bug_Tracker
Menu, HelpMenu, Add, About, LaunchAboutDialog
Menu, Tray, Add, Help, :HelpMenu
Menu, Tray, Add
Menu, Tray, Add, Settings, :ToggleMenu
Menu, Tray, Add, Reload Tweaks, Reloader
Menu, Tray, Add
Menu, Tray, Add, Exit, GoExit
DetectHiddenText, off
SendMode Input

;unlock listener setup
Gui +LastFound
hwnd := WinExist() ; getting a handle to the script gui

DllCall("Wtsapi32.dll\WTSRegisterSessionNotification", "uint", hwnd, "uint", 0) ; registering the gui, but only for this session
OnMessage(0x02B1, "sessionChange") ; start listening

;Ticket Count GUI Setup
Gui, TicketCountGUI:+LastFound

Gui, TicketCountGUI:+ToolWindow +AlwaysOnTop
Gui, TicketCountGUI:Margin, 0, 0                         
Gui, TicketCountGUI:Font, S32 CDefault Bold, Verdana
Gui, TicketCountGUI:Add, Text, Center vTicketCount, 00
Gui, TicketCountGUI:Font, S12, Verdana


;Vars

#Include Constants.ahk

Default_DB_URL := "https://www.dropbox.com/s/fhxij140x08jqoy/UD_DB.csv?dl=1"

IfNotExist, %A_AppData%/CompassAHKTweaks
{
	FileCreateDir, %A_AppData%/CompassAHKTweaks
}
SetWorkingDir, %A_AppData%/CompassAHKTweaks

IfNotExist, %A_WorkingDir%\CompassTweaks.ini
{
	IniWrite, %Default_DB_URL%, CompassTweaks.ini, Database, URL
	IniWrite, %parseCompleted%, CompassTweaks.ini, Database System Map, parseCompleted
	MsgBox, 4, Compass AHK Tweaks, Would you like to add a shortcut to your startup folder?
	IfMsgBox Yes
	{
		GoSub, CreateStartupShortcut
	}
	GoSub, Reloader
} else {
	IniRead, UDEXE, CompassTweaks.ini, Universal Desktop, Location, 1234
	IniRead, UPCURL, CompassTweaks.ini, Global Hotkeys, UPCURL, "http://api.upcdatabase.org/json/fd6b98ddad3f4110ca99829b9aa6f2b4/"
	IniRead, DB_URL, CompassTweaks.ini, Database, URL, 1234
	IniRead, parseCompleted, CompassTweaks.ini, Database System Map, ParseCompleted, 1234
	IniRead, UDTweaksEnabled, CompassTweaks.ini, Application Tweaks, UD, true
	IniRead, UDTimeoutExtensionEnabled, CompassTweaks.ini, Application Tweaks, UD Timeout Disabled, true
	IniRead, PCICompliantIdle, CompassTweaks.ini, Application Tweaks, UD Timeout PCI Compliant, true
	IniRead, EMCTweaksEnabled, CompassTweaks.ini, Application Tweaks, EMC, true
	IniRead, EMCOverrideBypassEnabled, CompassTweaks.ini, Application Tweaks, EMC Override Bypass, true
	IniRead, GlobalHotkeysEnabled, CompassTweaks.ini, Application Tweaks, GHK, true
	IniRead, SlackHotkeyEnabled, CompassTweaks.ini, Application Tweaks, Slack Hotkey, true
	IniRead, RemedyTweaksEnabled, CompassTweaks.ini, Application Tweaks, Remedy, true
	IniRead, BarcodeCopyToolsEnabled, CompassTweaks.ini, Application Tweaks, SKUTools, true
	IniRead, DebuggingEnabled, CompassTweaks.ini, Application Tweaks, Debugging, 1234
	IniRead, xPos, CompassTweaks.ini, Ticket Count Settings, XPOS, 0
	IniRead, yPos, CompassTweaks.ini, Ticket Count Settings, YPOS, 0
	IniRead, GUILocked, CompassTweaks.ini, Ticket Count Settings, LOCKED, 0
	IniRead, GUIShown, CompassTweaks.ini, Ticket Count Settings, Display, 0
	IniRead, UseMyDocuments, CompassTweaks.ini, Advanced Settings, Use_My_Documents, 0
	IniRead, LastUpdateDownload, CompassTweaks.ini, Updates, Last Updated, 1234
}

;Launch UD Idle Close Prevention if UD is running
Process, Exist, UniversalDesktop.exe
if (errorlevel) {
	SetTimer, PreventUDIdleClose, 15000
}

;SetTimer, testReady, 500

if (DebuggingEnabled = 1234) {
	DebuggingEnabled := False
	IniWrite, %DebuggingEnabled%, CompassTweaks.ini, Application Tweaks, Debugging
}

if (DebuggingEnabled) {
	try{
		Menu, Tray, Icon, %A_ScriptDir%/icons/emblem_marketing.ico
	} catch e {
		TrayTip, CompassAHKTweaks, unable to set icon
	}
	Menu, Tray, Tip, % TimeString "`nDebugging Enabled"
	connectionString := A_WorkingDir . "/Units.db"
	;db := DBA.DataBaseFactory.OpenDataBase("SQLite", connectionString)
} else {
	Menu, Tray, Tip, %TimeString%
}

if DebuggingToggleEnabled or DebuggingEnabled
{
	Menu, DebugMenu, Add, Enable Debugging, DebugEnabledToggle
	Menu, ToggleMenu, Add, Debugging, :DebugMenu
	Menu, DebugMenu, Add, Ready Test, testReady
}

if UseMyDocuments {
	ticketCountIni := A_MyDocuments . "/CAHKT_Files/ticket_count.ini"
	csvDir := A_MyDocuments . "/CAHKT_Files"
	TrayTip, CompassAHKTweaks, % "My Documents mode enabled for sync support."
	Sleep, 500
	IfNotExist, csvDir
		FileCreateDir, %csvDir%
} else {
	ticketCountIni := A_WorkingDir . "/ticket_count.ini"
	csvDir := A_WorkingDir
}

if (DB_URL != Default_DB_URL) {
	IniWrite, %Default_DB_URL%, CompassTweaks.ini, Database, URL
	DB_URL := Default_DB_URL
}
	
if (UDEXE != 1234) {
	GoSub,DownloadUpdatedDB
	GoSub,PruneArchive
	if (parseCompleted = 1234) {
		ParseSystemMap()
	}
}

if (LastUpdateDownload = 1234) {
	LastUpdateDownload := A_Now
	IniWrite, %A_Now%, CompassTweaks.ini, Updates, Last Updated
}

if (!DebuggingEnabled)
	; CheckForUpdate(LastUpdateDownload)
lastUnitNumber := ""

if UDTweaksEnabled
	Menu, UDToggleMenu, Check, Enable UD Tweaks

if UDTimeoutExtensionEnabled
	Menu, UDToggleMenu, Check, Enable UD Timeout Extension

if EMCTweaksEnabled
	Menu, EMCToggleMenu, Check, Enable EMC Tweaks

if GlobalHotkeysEnabled
	Menu, ToggleMenu, Check, Enable Global Hotkeys

if BarcodeCopyToolsEnabled
	Menu, ToggleMenu, Check, Enable Global Barcode Tools
	
if EMCOverrideBypassEnabled
	Menu, EMCToggleMenu, Check, Enable EMC Override Bypass
	
if DebuggingEnabled
	Menu, DebugMenu, Check, Enable Debugging
	
if SlackHotkeyEnabled
	Menu, ToggleMenu, Check, Enable Slack Hotkey
	
if GUIShown
{
	GoSub, UpdateTicketCount
	GuiControl, TicketCountGUI:, TicketCount, %TicketCount%
	GoSub, DisplayTicketCountGUI
}

SetTimer, TeamViewerPopUpWatcher, 250

#Include Global_Hotkeys.ahk
; #Include AutoType.ahk
#Include EMC_XML_Functions.ahk
#Include UD_Tweaks.ahk
#Include EMC_Tweaks.ahk
#Include Remedy_Tweaks.ahk
; #Include Update_Check.ahk
	
GlobalHKFlagToggle:
	if (GlobalHotkeysEnabled) {
		GlobalHotkeysEnabled := false
		Menu, ToggleMenu, Uncheck, Enable Global Hotkeys
	} else {
		GlobalHotkeysEnabled := true
		Menu, ToggleMenu, Check, Enable Global Hotkeys
	}
	IniWrite, %GlobalHotKeysEnabled%, CompassTweaks.ini, Application Tweaks, GHK
return

SlackFlagToggle:
	if (SlackHotkeyEnabled) {
		SlackHotkeyEnabled := false
		Menu, ToggleMenu, Uncheck, Enable Slack Hotkey
	} else {
		SlackHotkeyEnabled := true
		Menu, ToggleMenu, Check, Enable Slack Hotkey
	}
	IniWrite, %GlobalHotKeysEnabled%, CompassTweaks.ini, Application Tweaks, Slack Hotkey
return

RemedyFlagToggle:
	if (RemedyTweaksEnabled) {
		RemedyTweaksEnabled := false
		Menu, ToggleMenu, Uncheck, Enable Remedy Tweaks
	} else {
		RemedyTweaksEnabled := true
		Menu, ToggleMenu, Check, Enable Remedy Tweaks
	}
	IniWrite, %RemedyTweaksEnabled%, CompassTweaks.ini, Application Tweaks, Remedy
return

UDFlagToggle:
	if (UDTweaksEnabled) {
		UDTweaksEnabled := false
		Menu, UDToggleMenu, Uncheck, Enable UD Tweaks
	} else {
		UDTweaksEnabled := true
		Menu, UDToggleMenu, Check, Enable UD Tweaks
	}
	IniWrite, %UDTweaksEnabled%, CompassTweaks.ini, Application Tweaks, UD
return

UDTimeoutToggle:
	if (UDTimeoutExtensionEnabled) {
		UDTimeoutExtensionEnabled := false
		Menu, UDToggleMenu, Uncheck, Enable UD Timeout Extension
	} else {
		UDTimeoutExtensionEnabled := true
		Menu, UDToggleMenu, Check, Enable UD Timeout Extension
	}
	IniWrite, %UDTimeoutExtensionEnabled%, CompassTweaks.ini, Application Tweaks, UD Timeout Disabled
return

EMCFlagToggle:
	if (EMCTweaksEnabled) {
		EMCTweaksEnabled := false
		Menu, EMCToggleMenu, Uncheck, Enable EMC Tweaks
	} else {
		EMCTweaksEnabled := true
		Menu, EMCToggleMenu, Check, Enable EMC Tweaks
	}
	IniWrite, %EMCTweaksEnabled%, CompassTweaks.ini, Application Tweaks, EMC
return

EMCOverrideBypassToggle:
	if (EMCOverrideBypassEnabled) {
		EMCOverrideBypassEnabled := false
		Menu, EMCToggleMenu, Uncheck, Enable EMC Override Bypass
	} else {
		EMCOverrideBypassEnabled := true
		Menu, EMCToggleMenu, Check, Enable EMC Override Bypass
	}
	IniWrite, %EMCOverrideBypassEnabled%, CompassTweaks.ini, Application Tweaks, EMC Override Bypass
return

BarcodeToolsToggle:
	if (BarcodeCopyToolsEnabled) {
		BarcodeCopyToolsEnabled := false
		Menu, ToggleMenu, Uncheck, Enable Global Barcode Tools
	} else {
		BarcodeCopyToolsEnabled := true
		Menu, ToggleMenu, Check, Enable Global Barcode Tools
	}
	IniWrite, %BarcodeCopyToolsEnabled%, CompassTweaks.ini, Application Tweaks, SKUTools
return

DebugEnabledToggle:
	if (DebuggingEnabled) {
		DebuggingEnabled := false
		Menu, DebugMenu, Uncheck, Enable Debugging
	} else {
		DebuggingEnabled := true
		Menu, DebugMenu, Check, Enable Debugging
	}
	IniWrite, %DebuggingEnabled%, CompassTweaks.ini, Application Tweaks, Debugging
return

LaunchAboutDialog:
	ShowAboutGUI(PROG_VERSION)
return

ResetTicketCounterLocation:
	xPos := A_ScreenWidth - 73
	yPos := A_ScreenHeight - 125
	msgBox, % "Location has been reset."
	GoSub,MonitorBypass
return

DisplayTicketCountGUI:
	sysGet, monitors, MonitorCount
	if (monitors > 1) {
		IniRead, xPos, CompassTweaks.ini, Ticket Count Settings, XPOS_Docked, 0
		IniRead, yPos, CompassTweaks.ini, Ticket Count Settings, YPOS_Docked, 0
	} else {
		IniRead, xPos, CompassTweaks.ini, Ticket Count Settings, XPOS_solo, 0
		IniRead, yPos, CompassTweaks.ini, Ticket Count Settings, YPOS_solo, 0
	}
MonitorBypass:
	if (xPos = 0) or (yPos = 0) {
		GoSub,ResetTicketCounterLocation
	} else {
		Gui, TicketCountGUI:+LastFound
		if GUILocked
		{
			Gui, TicketCountGUI:-Caption
			Gui, TicketCountGUI:+AlwaysOnTop
			WinSet, TransColor, FFFFFF 175
			Winset, ExStyle, +0x20
		} else {
			Gui, TicketCountGUI:+Caption
			WinSet, TransColor, Off
			Winset, ExStyle, -0x20
		}
		try {
			Gui, TicketCountGUI:Show, X%xPos% Y%yPos%, Ticket Count
		} catch e {
			MsgBox % "Invalid X/Y Coordinates`nX:" xPos "`nY:" yPos "`n`nResetting"
			GoSub,ResetTicketCounterLocation
		}
		GUIShown := true
		IniWrite, %GUIShown%, CompassTweaks.ini, Ticket Count Settings, Display
		GuiControl, TicketCountGUI:, TicketCount, %TicketCount%
	}
return

UpdateTicketCount:
	FormatTime, iniDate, %A_Now%, yyMMdd
	IniRead, ticketCount, %ticketCountIni%, Ticket Count, %iniDate%, 0
	GuiControl, TicketCountGUI:, TicketCount, %TicketCount%
return

HideTicketCountGUI:
	Gui, TicketCountGUI:Show, Hide
	GUIShown := false
	IniWrite, %GUIShown%, CompassTweaks.ini, Ticket Count Settings, Display
return

LockTicketCountGUI:
	sysGet, monitors, MonitorCount
	WinGetPos, xPos, yPos,,,Ticket Count
	if (monitors > 1) {
		IniWrite, %xPos%, CompassTweaks.ini, Ticket Count Settings, XPOS_Docked
		IniWrite, %yPos%, CompassTweaks.ini, Ticket Count Settings, YPOS_Docked
	} else {
		IniWrite, %xPos%, CompassTweaks.ini, Ticket Count Settings, XPOS_Solo
		IniWrite, %yPos%, CompassTweaks.ini, Ticket Count Settings, YPOS_Solo
	}
	GUILocked := true
	if GUIShown
		GoSub, DisplayTicketCountGUI
return
	
UnlockTicketCountGUI:
	Gui, TicketCountGUI:+Caption
	Gui, TicketCountGUI:+LastFound
	WinSet, TransColor, OFF
	GUILocked := false
return

ToggleLockTicketCountGUI:
	if GUILocked
		Gosub, UnlockTicketCountGUI
	Else
		Gosub, LockTicketCountGUI
	IniWrite, %GUILocked%, CompassTweaks.ini, Ticket Count Settings, LOCKED
return

ToggleTicketCountGUI:
	if GUIShown
		GoSub, HideTicketCountGUI
	else
		GoSub, DisplayTicketCountGUI
return

LocateUD:
	msgBox,64,First Run, Please select the location of Universal Desktop when the file selector appears.
	FileSelectFile, UDEXE, 3,, Universal Desktop Location
	If ErrorLevel {
		msgBox, 48, Error, You must specify Universal Desktop Location to continue.
		;ExitApp
	}
	IniWrite, %UDEXE%, CompassTweaks.ini, Universal Desktop, Location
return

;test for missing leading zero
CorrectBarcode(x)
{
	StringLen, length, x
	finaldigit := SubStr(x, length, 1)
	y := "0" . SubStr(x, 1, length - 1)
	checkdigit := CalcChecksum(y)
	if (checkdigit = finaldigit)
		TrayTip, CompassAHKTweaks, Barcode could me missing a leading zero`n0%x%, 10, 1
	return x . CalcChecksum(x)
}

;calculate missing check digit from 11-digit barcode
CalcChecksum(x)
{
	StringLen, length, x
	oddsum = 0
	evensum = 0
	checksum = 0
	tempsum = 0
	counter = 0
	
	Loop %length% {
		counter++
		sub := SubStr(x, counter, 1)
		if (Mod(counter,2) = 0) {
			evensum := evensum + sub 
		} else {
			oddsum := oddsum + sub
		}
	}
	oddsum := oddsum * 3
	tempsum := Mod((oddsum + evensum),10)
	if (tempsum > 0) {
		checksum := (10 - tempsum)
	} else {
		checksum := tempsum
	}
	return checksum
}

ParseSystemMap:
	GoSub, ParseSystemMap
return

ParseSystemMap()
{
	MsgBox, 4099, CompassAHKTweaks, We need to parse your Universal Desktop setup for system names.  If Universal Desktop is currently open, this will close it.`nDo you want to continue?
	IfMsgBox, Yes
	{
		;IfExist, %A_WorkingDir%\UD_DB.csv
		;{
		;	IfNotExist, %A_WorkingDir%\DB_Archive
		;	{
		;		FileCreateDir, %A_WorkingDir%\DB_Archive
		;	}
		;	FileCopy, %A_WorkingDir%\UD_DB.csv, %A_WorkingDir%\DB_Archive\UD_DB-old.csv, 1
		;	FileDelete, %A_WorkingDir%\UD_DB.csv
		;}
		GoSub,BulletProofUDLaunch
		WinWait, Login ahk_class WindowsForms10.Window.8.app.0.33c0d9d,Loading..., 5
		WinWaitClose, Login ahk_class WindowsForms10.Window.8.app.0.33c0d9d,Loading..., 5
		TrayTip, CompassAHKTweaks, Preparing to gather systems info
		Sleep, 1000
		ControlGet, SystemList, List,, WindowsForms10.COMBOBOX.app.0.33c0d9d1, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
		Loop, Parse, SystemList, `n
		{
			Control, ChooseString, %A_LoopField%, WindowsForms10.COMBOBOX.app.0.33c0d9d1, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
			Sleep,500
			WinGetText, winText, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
			RegExMatch(winText, "(?<=//).*?(?=\.pos)", systemURL)
			Sleep, 300
			IniWrite, %A_LoopField%, CompassTweaks.ini, Database System Map, %systemURL%
			Sleep, 300
		}
		RegExMatch(systemText, "\D*(?=\d)", prefix)
		GoSub,KillUDLogin
		IniWrite, 1, CompassTweaks.ini, Database System Map, ParseCompleted
		MsgBox, Scrape Completed Successfully!
	}
	
	IfMsgBox, No
	{
		MsgBox, % "Scrape skipped.  Will try again next time. (HINT: If you don't want to see this everytime, press cancel to abort the UD scrape permanently."
	}
	
	IfMsgBox, Cancel
	{
		MsgBox, 4100, CompassAHKTweaks, % "If you cancel the scrape, auto logins may not work properly for Universal Desktop.  Are you sure you want to cancel?"
		
		IfMsgBox, Yes
		{
			IniWrite, 1, CompassTweaks.ini, Database System Map, ParseCompleted
		}
	}
}

CreateStartupShortcut:
	FileCreateShortcut, %A_ScriptFullPath%, %A_Startup%\%A_ScriptName%.lnk, %A_ScriptDir%
	If (ErrorLevel) {
		MsgBox, Failed to create shortcut.
	} else {
		MsgBox, %A_ScriptName% will now start with windows automatically.
	}
return
		
JSONTest:
	TestUPCURL := UPCURL . barcode
	MsgBox, The URL was %UPCURL% and barcode was %barcode%`nNew URL is %TestUPCURL%
	UrlDownloadToFile, %TestUPCURL%, %A_WorkingDir%\temp.json
return

DownloadUpdatedDB:
	IfExist, %A_WorkingDir%\UD_DB.csv
	{
		IfExist, %A_WorkingDir%\UD_DBx.csv
		{
			FileDelete, %A_WorkingDir%\UD_DBx.csv
		}
		UrlDownloadToFile, %DB_URL%, %A_WorkingDir%\UD_DBx.csv
		databaseStatus := VerifyDB(A_WorkingDir . "\UD_DBx.csv")
		if (!databaseStatus) {
			TrayTip, CompassAHKTweaks, Universal Desktop Database Update file downloaded.
			GoSub,UpdateDB
		} else {
			IfExist, %A_WorkingDir%\UD_DBx.csv
			{
				FileDelete, %A_WorkingDir%\UD_DBx.csv
			}
			If UseMyDocuments {
				FileGetTime, appDataCSVTime, %A_WorkingDir%\UD_DB.csv
				FileGetTime, docCSVTime, %csvDir%\UD_DB.csv
				If (appDataCSVTime > docCSVTime) {
					FileCopy, %A_WorkingDir%\UD_DB.csv, %csvDir%\UD_DB.csv, 1
					;TrayTip, CompassAHKTweaks, % "Copied appdata csv to myDocuments"
				} else {
					FileCopy, %csvDir%\UD_DB.csv, %A_WorkingDir%\UD_DB.csv, 1
					;TrayTip, CompassAHKTweaks, % "Copied myDocuments csv to AppData"
				}
			}
		}
	} else {
		UrlDownloadToFile, %DB_URL%, %A_WorkingDir%\UD_DB.csv
		databaseStatus := VerifyDB(A_WorkingDir . "\UD_DB.csv")
		if (!databaseStatus) {
			TrayTip, CompassAHKTweaks, Unable to download latest DB file.  Please Try Again Later.
		}
		FileDelete, %A_WorkingDir%\UD_DB.csv
	}
return

ManualDBUpdate:
	FileSelectFile, DB_Update, 3,, Select Universal Desktop Database File, *.csv
	FileCopy, DB_Update, %A_WorkingDir%\UD_DBx.csv
	GoSub,UpdateDB
return

UpdateDB:
	IfExist, %A_WorkingDir%\UD_DBx.csv
	{
		IfNotExist, %A_WorkingDir%\DB_Archive
		{
			FileCreateDir, %A_WorkingDir%\DB_Archive
		}
		FormatTime, TimeAppendix, %A_Now%, yyMMdd_HHmmss
		archiveFile := "UD_DB-" . TimeAppendix . ".csv"
		FileCopy, %A_WorkingDir%\UD_DB.csv, %A_WorkingDir%\DB_Archive\%archiveFile%, 1
		IntegrateOrReplaceCSV("UD_DB.csv", "UD_DBx.csv")
	} else {
		msgBox, No New UD_DBx found.
	}
	If UseMyDocuments {
		FileGetTime, appDataCSVTime, %A_WorkingDir%\UD_DB.csv
		FileGetTime, docCSVTime, %csvDir%\UD_DB.csv
		If (appDataCSVTime > docCSVTime) {
			FileCopy, %A_WorkingDir%\UD_DB.csv, %csvDir%\UD_DB.csv, 1
			TrayTip, CompassAHKTweaks, % "Copied appdata csv to myDocuments"
		} else {
			FileCopy, %csvDir%\UD_DB.csv, %A_WorkingDir%\UD_DB.csv, 1
			TrayTip, CompassAHKTweaks, % "Copied myDocuments csv to AppData"
		}
	}
return

IntegrateOrReplaceCSV(currentFile, newFile)
{
	if (VerifyDB(currentFile) = True) {
		if (ParseCSVForVersion(currentFile) < ParseCSVForVersion(newFile)) {
			GoSub,ReplaceCSV
		} else {
			GoSub,CallPythonDBIntegration
		}
	} else {
		GoSub,ReplaceCSV
	}
	return
}

ParseCSVForVersion(file)
{
	version := ""
	FileReadLine, firstLine, % file, 1
	csvDetails := StrSplit(firstLine, .)
	if (csvDetails.maxIndex() > 1) {
		version := csvDetails[2]
	}
	
	return version
}
	
ReplaceCSV:
	FileDelete, %A_WorkingDir%\UD_DB.csv
	FileMove, %A_WorkingDir%\UD_DBx.csv, %A_WorkingDir%\UD_DB.csv
	TrayTip, CompassAHKTweaks, UD_DBx successfully Replaced!
return

CallPythonDBIntegration:
	RunWait,%A_ScriptDir%/Delta/DatabaseDeltaSync.exe
	FileDelete, %A_WorkingDir%\UD_DBx.csv
	TrayTip, CompassAHKTweaks, UD_DBx successfully Updated!
return

PruneArchive:
	fileCount = 0
	recentFileTime1 = 0
	recentFile1 := ""
	recentFile2 := ""
	recentFileTime2 = 0
	recentFile3 := ""
	recentFileTime3 = 0
	fileDeleteCount = 0
	
	Loop, %A_WorkingDir%\DB_Archive\*.csv
	{
		fileCount++
		if (A_LoopFileTimeModified > recentFileTime3) {
			if (A_LoopFileTimeModified > recentFileTime2) {
				if (A_LoopFileTimeModified > recentFileTime1) {
					recentFile1 := A_LoopFileName
					recentFiletime1 := A_LoopFileTimeModified
					continue
				}
				recentFile2 := A_LoopFileName
				recentFileTime2 := A_LoopFileTimeModified
				continue
			}
			recentFile3 := A_LoopFileName
			recentFileTime3 := A_LoopFileTimeModified
			continue
		} else {
			FileDelete, %A_LoopFileFullPath%
			fileDeleteCount++
		}
	}
	
	Loop, %A_WorkingDir%\DB_Archive\*.csv
	{
		if (A_LoopFileName != recentFile1 && A_LoopFileName != recentFile2 && A_LoopFileName != recentFile3){
			FileDelete, %A_LoopFileFullPath%
			fileDeleteCount++
		}
	}
return

Display_HotkeyList:
	Run, %A_ScriptDir%\hotkeys_list.txt
return

Go_Bug_Tracker:
	Run, https://bitbucket.org/compass_dataservices/compassahktweaks/issues?status=new&status=open
return

BulletProofUDLaunch:
	if (UDEXE = 1234) {
		GoSub,LocateUD
	}
	
	Process, Exist, UniversalDesktop.exe
	if (errorlevel) {
		Process, Close, %errorlevel%
		Process, WaitClose, %errorlevel%, 5
		if (errorlevel) {
			msgBox Unable to kill UD
			return
		}
	}
	Try {
		Run, %UDEXE%
	} Catch e {
		msgBox, There was a problem launching UD.
		GoSub,LocateUD
		GoSub, %A_ThisLabel%
	}
return

KillUDLogin:
	WinWait, Login
	WinClose, Login
return

RemoveToolTip:
	SetTimer, RemoveToolTip, Off
	ToolTip
return

#Include Help_GUI.ahk

GoExit:
ExitApp

ShowAboutGUI(version)
{
	Gui, AboutGUI: New
	Gui, AboutGUI:Add, Text,, % "CompassAHKTweaks v" . version
	; Generated using SmartGUI Creator 4.0
	Gui, AboutGUI:+ToolWindow
	Gui, AboutGUI:Show, AutoSize Center, About
	Return
}

VerifyDB(file)
{
	FileReadLine, firstLine, % file, 1
	IfInString, firstLine, #
	{
		return true
	} else {
		return false
	}
}

DateToDay(date)
{
	year := SubStr(date, 1, 4)
	month := SubStr(date, 5, 2)
	day := SubStr(date, 7, 2)
	if (year > 1900) and (0 < month < 13) and (0 < day < 31) {
		FormatTime, dayOfWeek, % date, ddd
		return dayOfWeek
	} else {
		return "acketz"
	}
}

Date2Day(month, day, year)
{
	if (month < 10)
		month := "0" . month
	if (day < 10)
		day := "0" . day
	dateString := year . month . day
	result := DateToDay(dateString)
	return result
}

;Code block necessary for sending special characters format: SendUTF8("Text", 1) 
SendUTF8(Data, Raw = 0)
{
   SaveFormat := (uint32)A_FormatInteger
   SetFormat IntegerFast, H
   Loop Parse, Data
   {
      If (BytesLeft > 0)
      {
         CodePoint := (CodePoint << 6) + (Asc(A_LoopField) & 63)
         If (--BytesLeft = 0)
            OutPut .= "{U+" SubStr(CodePoint, 3) "}"
      }
      Else
      {
         If Asc(A_LoopField) < 192
            If (Raw AND InStr("!#+^{}", A_LoopField))
               OutPut .= "{" A_LoopField "}"
            Else
               OutPut .= A_LoopField
         Else If Asc(A_LoopField) < 224
            CodePoint := Asc(A_LoopField) & 31, BytesLeft := 1
         Else If Asc(A_LoopField) < 240
            CodePoint := Asc(A_LoopField) & 15, BytesLeft := 2
         Else
            CodePoint := Asc(A_LoopField) & 7, BytesLeft := 3
      }
   }
   Send %OutPut%
   SetFormat IntegerFast, %SaveFormat%
}

;Barcode Trimming Method
trimBarcode()
{
	Critical
	oldClip=
	oldClip := ClipboardAll
	Sleep, 100
	while (RegExMatch(Clipboard, "^0\d*")) {
		RegExMatch(Clipboard, "(?<=^0)(\d|-|\s)*", match)
		if (match) {
			Clipboard := match
			Sleep,300
		}
	}
	
	StringReplace, clipboard, clipboard, -,,All
	StringReplace, clipboard, clipboard, %A_Space%,,All
	
	Send ^v
	Sleep,300
	if (oldClip) {
		Clipboard=
		Clipboard = %oldClip%
		ClipWait,1
	}
}

testReady:
	testReady()
return

testReady()
{
	;TrayTip, DEBUG, Testing Phone Queue GUI Display
	IfWinExist, ahk_exe agent.exe
	{
		;TrayTip, Phone Queue, Agent Running
		IfWinExist, Ready - Cisco Agent Desktop
		{
			;TrayTip, Phone Queue, Setting GUI to show Ready - Green
			displayPhoneQueueStatus("Ready")
		}
		IfWinExist, Not Ready - Cisco Agent Desktop
		{
			;TrayTip, Phone Queue, Setting GUI to show Not Ready - Red
			displayPhoneQueueStatus("Not Ready")
		}
		IfWinExist, Logout - Cisco Agent Desktop
		{
			displayPhoneQueueStatus("Logout")
		}
		IfWinExist, Talking
		{
			displayPhoneQueueStatus("Talking")
		}
	} else {
		IfWinExist, Cisco Finesse - Cisco Finesse
		{
			IfWinActive, Cisco Finesse - Cisco Finesse
			{
				Global queueStatusColorA
				MouseGetPos, mouseX, mouseY
				PixelGetColor, queueStatusColorA, 91, 96
				;TrayTip, Phone Queue, % "Queue Status:" queueStatusColor
			}
			WinGet, ciscoWin, ID, Cisco Finesse - Cisco Finesse
			;queueStatusColor := Gdip_BitmapFromHWND(ciscoWin)
			;displayPhoneQueueStatus(queueStatusColorA)
			;TrayTip, Phone Queue, Last Pixel Color:%queueStatusColorA%`nGDI Color:%queueStatusColor%
		} else {
			displayPhoneQueueStatus("Closed")
		}
	}
}

displayPhoneQueueStatus(queueStatus)
{
	;TrayTip, Display Queue Status, %queueStatus%
	Gui, TicketCountGUI:+LastFound
	if (queueStatus == "Ready"){
		Gui, TicketCountGUI:Color, Green
	} else if (queueStatus == "Not Ready") {
		Gui, TicketCountGUI:Color, Red
	} else if (queueStatus == "Logout") {
		Gui, TicketCountGUI:Color, Gray
	} else if (queueStatus == "Talking") {
		Gui, TicketCountGUI:Color, Yellow
	} else {
		Gui, TicketCountGUI:Color, %queueStatus%
	}
	;GoSub, DisplayTicketCountGUI
}

sessionChange(wparam, lparam, msg, hwnd)
{
	global GUIShown
	if GUIShown
	{
		GoSub, UpdateTicketCount
	}
}

Unregister:
	DllCall("Wtsapi32.dll\WTSUnRegisterSessionNotification", "uint", hwnd) ; unregister
ExitApp ; and exit