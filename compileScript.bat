@ECHO OFF
IF EXIST CompassAHKTweaks.exe (
	DEL CompassAHKTweaks.exe
)

IF EXIST CompassAHKTweaks_Setup.exe (
	DEL CompassAHKTweaks_Setup.exe
)

IF %ERRORLEVEL% GTR 0 (
	Echo Can't remove existing files.
	Pause
	END
)

@ECHO ON

"C:/Program Files/AutoHotKey/Compiler/Ahk2Exe.exe" /in AutoHotKey_Compass.ahk /out CompassAHKTweaks.exe /icon ./icons/emblem_marketing.ico /bin "C:/Program Files/AutoHotKey/Compiler/ANSI 32-bit.bin"

"C:/Program Files/AutoHotKey/Compiler/Ahk2Exe.exe" /in uberClipboard.ahk /out uberClipboard.exe /bin "C:/Program Files/AutoHotKey/Compiler/ANSI 32-bit.bin"

"C:/Program Files/AutoHotKey/Compiler/Ahk2Exe.exe" /in StoreInfoGUI.ahk /out StoreInfoGUI.exe /bin "C:/Program Files/AutoHotKey/Compiler/ANSI 32-bit.bin"

@ECHO OFF
IF EXIST "C:\Program Files (x86)\Microsoft SDKs\Windows\v7.1A\Bin\signtool.exe" (
	"C:\Program Files (x86)\Microsoft SDKs\Windows\v7.1A\Bin\signtool" sign /a CompassAHKTweaks.exe
	"C:\Program Files (x86)\Microsoft SDKs\Windows\v7.1A\Bin\signtool" sign /a uberClipboard.exe
	"C:\Program Files (x86)\Microsoft SDKs\Windows\v7.1A\Bin\signtool" sign /a StoreInfoGUI.exe
) ELSE (
	ECHO Unable to sign exe files automatically.  Please sign before continuing.
	PAUSE
)

IF %ERRORLEVEL% LEQ 0 (
	ECHO Executables successfully signed.
)

IF EXIST "%PROGRAMFILES(X86)%" (
	IF EXIST "C:\Program Files (x86)\NSIS\makensis.exe" (
		"C:\Program Files (x86)\NSIS\makensis.exe" CompassAHKTweaks.nsi
	) ELSE (
		Echo Couldn't locate NSI executable. Aborting.
		PAUSE
		END
	)
) ELSE (
	IF EXIST "C:\Program Files\NSIS\makensis.exe" (
		"C:\Program Files\NSIS\makensis.exe" CompassAHKTweaks.nsi
	) ELSE (
		Echo Couldn't locate NSI executable. Aborting.
		PAUSE
		END
	)
)

ECHO.
IF %ERRORLEVEL% GTR 0 (
	Echo Something went wrong.
	ECHO.
	Pause
) ELSE (
	Echo Success!
)