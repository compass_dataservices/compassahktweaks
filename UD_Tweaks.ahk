;;;;;;;;;;;;;;;;;;;;;;;
;  Universal Desktop  ;
;;;;;;;;;;;;;;;;;;;;;;;

;WindowsForms10.Window.8.app.0.33c0d9d87 = first list item
;WindowsForms10.BUTTON.app.0.33c0d9d2 = ID Checkbox
;WindowsForms10.Window.8.app.0.33c0d9d7 = SearchID field in filter buttons

#Include UD_AutoLogin.ahk

LastUDTab = 0

;non-login UD Window
#IfWinActive ahk_group UDGroup

;Grab Current Control Name
^!g::
	if (DebuggingEnabled && UDTweaksEnabled) {
		ControlGetFocus, ControlName
		if ErrorLevel
			MsgBox, The target window doesn't exist or none of its controls has input focus.
		else {
			MsgBox, Control with focus = %ControlName%
			clipboard=%ControlName%
		}
	}
return

;copy price across price levels
^2::
	if (DebuggingEnabled && UDTweaksEnabled) {
		Clipboard=
		Sleep 100
		ControlFocus, WindowsForms10.EDIT.app.0.33c0d9d10, A
		Sleep 100
		Send {HOME}
		Sleep 100
		Send +{END}
		Sleep 250
		Send ^c
		ClipWait,2
		ControlBase = WindowsForms10.EDIT.app.0.33c0d9d
		i = 10
		Loop, 12
		{
			EditControl := ControlBase . i
			ControlFocus, %EditControl%
			Send ^v
			Sleep 100
			i += 2
		}
	}
return

;test setting price
^3::
	if (DebuggingEnabled && UDTweaksEnabled) {
		TrayTip, CompassAHKTweaks, Setting price
		ControlGetFocus, c
		Send !e
		Sleep, 300
		Send {TAB}
		Sleep, 100
		ControlGetFocus, c2
		ControlSetText, %c2%, $1.59, ahk_exe UniversalDesktop.exe
		Sleep, 300
		Send !s
		Sleep, 2000
		ControlFocus, %c%
		TrayTip, CompassAHKTweaks, Focus returned to %c%
	}
return

^4::
	if (DebuggingEnabled && UDTweaksEnabled) {
		TrayTip, DEBUG, Custom Price Setting
		Send !e
		Sleep, 300
		Send {Tab}
		Send 1.59
		Sleep, 300
		Send !s
		GoSub, WaitForCursor
		GoSub, UDItemIDFocus
	}
return

^5::
	SendLevel, 3
	Send ^+{Ins}
	SendLevel, 0
return

;test report setup
^6::
	Send {Tab}
	Send {Space}
	Send {Left}
	Send {Down}
return
	

^v::
	GoSub,UDCorrectPaste
return

;Jump to (assign price level) box
Ins::
	if (UDTweaksEnabled) {
		WinGetText, UDWindowText, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
		StringGetPos, PLPos, UDWindowText, Price Levels
		if (PLPos > 1) {
			Loop
			{
				field := 10 + A_Index
				editor := "Edit" . field
				ControlFocus, %editor%
				if ErrorLevel
					break
				Sleep, 10
			}
		}
	}
return

;close tab
^w::
	if (UDTweaksEnabled) {
		MouseGetPos, MousePosX, MousePosY
		WinGetPos,,,WinWidth,WinHeight, A
		XClick := WinWidth - 15
		Click %XClick%,95
		MouseMove, MousePosX, MousePosY,0
	}
return

;tab cycling
^TAB::
	if (UDTweaksEnabled) {
		ControlFocus, %UD_ITEM_TABS%
		Send {Right}
	}
return

^+TAB::
	if (UDTweaksEnabled) {
		Send ^+{TAB}
	}
return

;WindowText test method
^!w::
	GoSub,GetCurrentTab
	msgBox,Item Pos = %itemTextPos%`nReport Pos = %ReportTextPos%`nScreen Pos = %ScreenTextPos%, 10, 1
return

!#w::
	ControlGetText, StatusBarText, WindowsForms10.Windows.8.app.0.33c0d9d142
	msgBox %StatusBarText%
return

;Ctrl+F quick search
^f::
UDSearch:
	if (UDTweaksEnabled) {
		GoSub,GetCurrentTab
		if (ItemTextPos = 0 || ReportTextPos = 0) {
			OldTitleMatchMode := A_TitleMatchMode
			SetTitleMatchMode 2
			ControlFocus, ID:
			Send {TAB 3}
			Send {DOWN}
			WinWait, ahk_class WindowsForms10.Window.808.app.0.33c0d9d, Sort Options,3
			GoSub,FocusSearchBox
			SetTitleMatchMode, %A_TitleMatchMode%
		} else if (ScreenTextPos = 0 || StoreContPos = 0 || ContainerPos = 0) {
			GoSub, ScreenQuickSearch
		}
	}
return

!f::
UDItemIDFocus:
	if (UDTweaksEnabled) {
		ControlFocus, %UD_ITEM_ID_TOP_EDIT%
		Send {End}
		Send +{Home}
	}
return

;Ctrl+S Save
^s::
	if (UDTweaksEnabled) {
		GoSub,GetCurrentTab
		if (ItemTextPos = 0) {
			Send !s
		} else {
			Send ^s
		}
	}
return

;Alt+A to Copy Next (Easy one-hand)
!a::
	if (UDTweaksEnabled) {
		GoSub,GetCurrentTab
		if (ItemTextPos = 0) {
			Send !n
		} else {
			Send !a
		}
	}
return

;Tab through price level fields easier
;WindowsForms10.EDIT.app.0.33c0d9d12
TAB::
	if (UDTweaksEnabled) {
		GoSub,GetCurrentTab
		if (ItemTextPos = 0) {
			ControlGetFocus, cName
			if (RegExMatch(cName, "i)edit\d+") or RegExMatch(cName, "WindowsForms10\.EDIT\.app.0\.33c0d9d\d{2,}")) {
				if (cName = UD_ITEM_PRICELEVEL_EDIT1) {
					Send {TAB}
				} else {
					Send {TAB 2}
					If ErrorLevel
					{
						Sleep, 500
						Send {TAB 2}
					}
				}
			} else {
				Send {TAB}
			}
		} else {
			Send {TAB}
		}
	} else {
		Send {TAB}
	}
return

;make reverse tab match
+TAB::
	if (UDTweaksEnabled) {
		GoSub,GetCurrentTab
		if (ItemTextPos = 0) {
			ControlGetFocus, cName
			if (RegExMatch(cName, "i)edit\d+") or RegExMatch(cName, "WindowsForms10\.EDIT\.app.0\.33c0d9d\d{2,}")) {
				Send +{TAB 2}
			} else {
				Send +{TAB}
			}
		} else {
			Send +{TAB}
		}
	} else {
		Send +{TAB}
	}
return

;Force Enter to work as expected in Advanced Search
;WindowsForms10.EDIT.app.0.33c0d9d1 = keyword in filter results
NumpadEnter::
Enter::
	if (UDTweaksEnabled) {
		ControlGetFocus, ControlName
		GoSub,GetCurrentTab
		if (ControlName = UD_ADVANCED_SEARCH_KEYWORD_EDIT) {
			Control, Check,, %UD_ADVANCED_SEARCH_CONTAINS_RADIO%
			ControlFocus, OK
			Send {SPACE}
		} else if (ControlName = "WindowsForms10.Window.8.app.0.33c0d9d7") {
			Control, Check,, WindowsForms10.BUTTON.app.0.33c0d9d2
			ControlFocus, OK
			Send {SPACE}
		} else if (ControlName = UD_ITEM_SEARCH_NAME_EDIT) {
			Send +{HOME}
			Send ^c
			Send {TAB}
			Send {DOWN}
			ControlFocus, %UD_ADVANCED_SEARCH_KEYWORD_EDIT%, ahk_class WindowsForms10.Window.808.app.0.33c0d9d
			Send ^v
		} else if (ControlName = UD_ITEM_ID_TOP_EDIT || ControlName = "WindowsForms10.Window.8.app.0.33c0d9d97" || ControlName = "WindowsForms10.Window.8.app.0.33c0d9d91") {
			GoSub,QuickSearch
		} else {
			Send {ENTER}
		}
	} else {
		Send {ENTER}
	}
return

;Trap F4 and provide warning
+!q::
	if (UDTweaksEnabled) {
		ControlSend, ahk_parent, {F4}, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
		If (ErrorLevel)
			ControlSend, ahk_parent, {F4}, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
	}
return

F4::
UDExit:
	SetTimer, PreventUDIdleClose, Off
	if (UDTweaksEnabled) {
		MsgBox, 4, Universal Desktop Tweaks, Are you sure you want to exit?
		IfMsgBox Yes
		{
			ControlSend, ahk_parent, {F4}, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
			If (ErrorLevel)
				ControlSend, ahk_parent, {F4}, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
			If (!keepLoginScreen)
				GoSub, KillUDLogin
		}
		else
			return
	}
	keepLoginScreen := False
return

^g::
if (DebuggingEnabled && UDTweaksEnabled) {
		ControlGetFocus, runButton, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
		if ErrorLevel
			MsgBox, The target window doesn't exist or none of its controls have input focus.
		else {
			TrayTip, CompassAHKTweaks, Run Button configured as %runButton%
		}
	}
return

;(\d{1,2})/(\d{1,2})/(\d{4})

dayCounter := 0
^r::
	if (DebuggingEnabled && UDTweaksEnabled && runButton != "") {
		; Loop, 100
		; {
			TrayTip, CompassAHKTweaks, Generating Report
			DailyReportGenerator(1, "10052 Product Mix-Revenue", runButton, True, 120)
			; TrayTip, CompassAHKTweaks, Completed %A_Index%/100
			; Sleep, 500
		; }
	}
return

^!r::
RunExcelReport:
	if (DebuggingEnabled && UDTweaksEnabled) {
		if (runButton != "") {
			ControlFocus, % runButton, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
			Send {Down}
			Send {Tab 2}
			Send {Space}
		}
	}
return

DailyReportGenerator(rType, rName, runButton, skipWeekends=True, loopCount=1)
{
	Loop, %loopCount%
	{
		TrayTip, CompassAHKTweaks, DailyReportGenerator
		if (runButton != "") {
			Clipboard=
			ControlFocus, Edit2, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
			Send ^c
			ClipWait, 1
			origDate := Clipboard
			parsedDate := ParseDate(origDate)
			if (rType = 0) {
				TrayTip, CompassAHKTweaks, Running Normal Report
				ControlFocus, % runButton, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
				Send {Space}
			} else if (rType = 1) {
				TrayTip, CompassAHKTweaks, Generating Excel Report
				ControlFocus, % runButton, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
				Send {Down}
				Send {Tab 2}
				Send {Space}
			} else {
				msgBox, % "Unsupported Report Type`nAborting"
			}
			WinWait, Generating
			WinWaitClose, Generating
			WinWait, % rName,,2
			WinActivate, % rName
			WinWaitActive, % rName
			Send ^+s
			WinWaitActive, Save As,, 2
			if (ErrorLevel) {
				Send !2
				WinWaitActive, Save As,, 10
				if (ErrorLevel) {
					return
				}
			}
			Sleep 1000
			ControlSend, Edit1, {End}, Save As
			ControlSend, Edit1, {Left 4}, Save As
			ControlSend, Edit1, {BS}, Save As
			ControlSend, Edit1, % parsedDate, Save As
			Clipboard := "C:\Users\beamar01\Documents\Projects\Unit 10052"
			ClipWait,1
			Sleep, 100
			ControlFocus, ToolbarWindow324, Save As
			Send, {Space}
			Sleep, 100
			Send, ^v
			ControlSend, UD_REPORT_SETUP_DATE_END, {Enter}, Save As
			Sleep, 500
			ControlFocus, Edit1, Save As
			ControlSend, UD_REPORT_SETUP_DATE_END, {Enter}, Save As
			Sleep, 100
			ControlSend, Edit1, {Enter}, Save As
			WinWaitClose, Save As
			Sleep, 500
			WinClose, % rName
			WinWaitClose, % rName
			WinActivate, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
			if ((skipWeekends = True and dayCounter < 4) or skipWeekends = False) {
				dayCounter += 1
				ControlSend, UD_REPORT_SETUP_DATE_START, {UP}, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
				ControlSend, UD_REPORT_SETUP_DATE_END, {UP}, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
			} else if (skipWeekends = True) {
				dayCounter := 0
				ControlSend, UD_REPORT_SETUP_DATE_START, {UP 3}, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
				ControlSend, UD_REPORT_SETUP_DATE_END, {UP 3}, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
			} 
			TrayTip, CompassAHKTweaks, Completed %A_Index%/%loopCount%
		} else {
			MsgBox, % "No configuration exists for run button."
		}
	}
return
}


;Synchronized date selection for reports
!UP::
^UP::
	if (UDTweaksEnabled) {
		GoSub,GetCurrentTab
		if (ReportTextPos = 0) {
			ControlSend, UD_REPORT_SETUP_DATE_START, {UP}, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
			ControlSend, UD_REPORT_SETUP_DATE_END, {UP}, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
		}
	}
return

!Right::
	if (UDTweaksEnabled) {
		GoSub,GetCurrentTab
		if (ReportTextPos = 0) {
			ControlSend, UD_REPORT_SETUP_DATE_START, {UP 7}, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
			ControlSend, UD_REPORT_SETUP_DATE_END, {UP 7}, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
		}
	}
return

^Right::
	if (UDTweaksEnabled) {
		GoSub,GetCurrentTab
		if (ReportTextPos = 0) {
			ControlSend, UD_REPORT_SETUP_DATE_START, {UP 30}, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
			ControlSend, UD_REPORT_SETUP_DATE_END, {UP 30}, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
		}
	}
return

!Down::
^Down::
	if (UDTweaksEnabled) {
		GoSub,GetCurrentTab
		if (ReportTextPos = 0) {
			ControlSend, UD_REPORT_SETUP_DATE_START, {DOWN}, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
			ControlSend, UD_REPORT_SETUP_DATE_END, {DOWN}, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
		}
	}
return

!Left::
if (UDTweaksEnabled) {
		GoSub,GetCurrentTab
		if (ReportTextPos = 0) {
			ControlSend, UD_REPORT_SETUP_DATE_START, {DOWN 7}, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
			ControlSend, UD_REPORT_SETUP_DATE_END, {DOWN 7}, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
		}
	}
return

^Left::
	if (UDTweaksEnabled) {
		GoSub,GetCurrentTab
		if (ReportTextPos = 0) {
			ControlSend, UD_REPORT_SETUP_DATE_START, {DOWN 30}, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
			ControlSend, UD_REPORT_SETUP_DATE_END, {DOWN 30}, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
		}
	}
return

;repeat arrow keys for easy button movement
Up::
Down::
Left::
Right::
	if (UDTweaksEnabled) {
		GoSub, GetCurrentTab
		if (ScreenTextPos = 0) {
			While GetKeyState(A_ThisHotkey, "P")
			{
				RepeatKey(A_ThisHotkey, A_Index)
			}
		} else {
			Send {%A_ThisHotkey%}
		}
	} else {
		Send {%A_ThisHotkey%}
	}
return

RepeatKey(key, repeatRate)
{
	Send {%key%}
	if (repeatRate < 10) {
		Sleep, 100
	} else if (repeatRate < 20) {
		Sleep, 50
	} else {
		Sleep, 10
	}
	return
}
	

;Grab variables for determining active tab
GetCurrentTab:
	WinGetText, UDWindowText, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
	StringGetPos, ItemTextPos, UDWindowText, Item
	StringGetPos, ReportTextPos, UDWindowText, Report Setup
	StringGetPos, ScreenTextPos, UDWindowText, Screen
	StringGetPos, StoreContPos, UDWindowText, Store Container
	StringGetPos, ContainerPos, UDWindowText, Container
return

;quick access to advanced search on screens tab
ScreenQuickSearch:
	OldTitleMatchMode := A_TitleMatchMode
	SetTitleMatchMode 3
	ControlFocus, Filter
	Send {SPACE}
	Send +{End}
	SetTitleMatchMode %A_TitleMatchMode%
return

SelectResult:
	WinWaitActive, ahk_class WindowsForms10.Window.8.app.0.33c0d9d,,2
	;WinWait, ahk_class #32770, The current search returns no results., 3
	Sleep 250
	Loop
	{
		IfEqual, A_Cursor, Arrow, break	;so long as we are still getting a non regular cursor, wait
		Sleep, 250
		test := A_Index
		if (A_Index > 40) {
			msgBox Search Timed Out!
			break
		}
	}
	WinActivate, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
	MouseGetPos, MousePosX, MousePosY
	Click 350,170
	MouseMove, MousePosX, MousePosY,0
return

#IfWinActive ahk_class WindowsForms10.Window.808.app.0.33c0d9d
;NOTES
;Search functions need to be condensed to subroutine and only variations processed for each hotkey combination

;Grab Current Control Name
^!g::
	if (DebuggingEnabled && UDTweaksEnabled) {
		ControlGetFocus, ControlName
		if ErrorLevel
		MsgBox, The target window doesn't exist or none of its controls has input focus.
		else {
			MsgBox, Control with focus = %ControlName%
			clipboard=%ControlName%
		}
	}
return

;return focus to search field and highlight contents
^f::
	if (UDTweaksEnabled) {
		ControlFocus, %UD_ADVANCED_SEARCH_KEYWORD_EDIT%, ahk_class WindowsForms10.Window.808.app.0.33c0d9d
		Send {HOME}
		Send +{END}
	}
return

;clean UPC formatting on paste
^v::
UDCorrectPaste:
	if (UDTweaksEnabled)
	{
		ControlGetFocus, cN
		if (cN = UD_ADVANCED_SEARCH_KEYWORD_EDIT)
		{
			trimBarcode()
		} else if (cN = UD_SKU_FIELD or cN = UD_SKU_FIELD2)
		{
			StringReplace, clipboard, clipboard, -,,All
			StringReplace, clipboard, clipboard, %A_Space%,,All
			Sleep, 300
			send ^v
		} else {
			send ^v
		}
	} else {
		send ^v
	}
return

;default search to contains
NumpadEnter::
$Enter::
	if (UDTweaksEnabled) {
		;check contains
		Control, Check,, %UD_ADVANCED_SEARCH_CONTAINS_RADIO%, ahk_class WindowsForms10.Window.808.app.0.33c0d9d
		GoSub, QuickSearch
	}
return

;Shift-Enter for search with SKU
+Enter::
	if (UDTweaksEnabled) {
		;check contains
		Control, Check,, %UD_ADVANCED_SEARCH_CONTAINS_RADIO%, ahk_class WindowsForms10.Window.808.app.0.33c0d9d
		;check sku
		Control, Check,, %UD_ADVANCED_SEARCH_SKU_CHECKBOX%, ahk_class WindowsForms10.Window.808.app.0.33c0d9d
		GoSub, QuickSearch
	}
return

;Ctrl-Enter for search begins with
^Enter::
	if (UDTweaksEnabled) {
		;check Begins with
		Control, Check,, %UD_ADVANCED_SEARCH_BEGINS_RADIO%, ahk_class WindowsForms10.Window.808.app.0.33c0d9d
		GoSub, QuickSearch
	}
return

;Alt-Enter for Search without SKU
!Enter::
	if (UDTweaksEnabled) {
		;check contains
		Control, Check,, %UD_ADVANCED_SEARCH_CONTAINS_RADIO%, ahk_class WindowsForms10.Window.808.app.0.33c0d9d
		;uncheck sku
		Control, Uncheck,, %UD_ADVANCED_SEARCH_SKU_CHECKBOX%, ahk_class WindowsForms10.Window.808.app.0.33c0d9d
		GoSub, QuickSearch
	}
return

PreventUDIdleClose:
	if (PCICompliantIdle == True) {
		idleTime := A_TimeIdlePhysical
	} else {
		idleTime := 0
	}
	IfWinExist Universal Desktop Idle Warning
	{
		If (idleTime < 10800)
		{
			ControlSend, Continue, {Enter}, Universal Desktop Idle Warning
			Sleep, 300
			IfWinExist Universal Desktop Idle Warning
			{
				msgBox, Failed to prevent UD timeout
			} else {
				Traytip, CompassAHKTweaks, Extended UD timemout
			}
		}
	}
return

QuickSearch:
	IfWinActive ahk_class WindowsForms10.Window.808.app.0.33c0d9d
	{
		GoSub,GetCurrentTab
		if (ItemTextPos = 0) {
			ControlFocus, %UD_ADVANCED_SEARCH_IDNUM_EDIT%, ahk_class WindowsForms10.Window.808.app.0.33c0d9d
			Send {BS}
			if (SearchCount() < 1) {
				MsgBox, No items found for search criteria.  Please try again.
				return
			} else {
				Send {Enter}
				GoSub, SelectResult
			}
		} else {
			ControlFocus, OK
			Send {SPACE}
		}
	} else {
		ControlGetFocus, c
		Send {Enter}
		if (c = UD_ITEM_ID_TOP_EDIT)
			GoSub, SelectResult
	}
return

FocusSearchBox:
	ControlFocus, %UD_ADVANCED_SEARCH_KEYWORD_EDIT%, ahk_class WindowsForms10.Window.808.app.0.33c0d9d
return

SearchCount()
{
	ControlSend, Count, {SPACE}, ahk_class WindowsForms10.Window.808.app.0.33c0d9d
	WinGetText, results, ahk_class WindowsForms10.Window.808.app.0.33c0d9d
	RegExMatch(results, "\d+(?=\sRecords)", resultCount)
	return %resultCount%
}

ParseDate(dateString)
{
	RegExMatch(dateString, "O)(\d{1,2})/(\d{1,2})/(\d{4})", dateObject)
	month := dateObject[1]
	day := dateObject[2]
	year := dateObject[3]
	if (month < 10) {
				month := "0" . month
			}
	if (day < 10) {
		day := "0" . day
	}
	return month . day . year
}


;WindowsForms10.EDIT.app.0.33c0d9d3 = PW box to monitor for {TAB}
;WindowsForms10.COMBOBOX.app.0.33c0d9d5 = combo box to select system