
;xmlDoc.setProperty("SelectionNamespaces", "xmlns:mouic='clr-namespace:Micros.OpsUI.Controls;assembly=OpsUI' xmlns='clr-namespace:EMC.Application.General.Controls.MicrosGridEditor;assembly=EMC'")




extractButton(xmlDoc, index=0)
{
	global BUTTON_XPATH
	allButtons := xmlDoc.selectNodes(BUTTON_XPATH)
	if index < 0
		return allButtons
	else
		return allButtons.item(index)
}

setMenuID(ByRef b, menuID)
{
	x := ComObjCreate("MSXML2.DOMDocument.6.0")
	nodeCommandNumber := x.createAttribute("OpsCommandNumber")
	nodeCommandNumber.value := menuID
	b.setAttributeNode(nodeCommandNumber)
}

getMenuID(b)
{
	isArray := true
	try {
		b_length := b.length()
	} catch e {
		isArray := false
	}
	
	if (isArray) {
		result := ""
		buttonArray := Object()
		for button in b
		{
			idNum := button.getAttribute("OpsCommandNumber")
			result := result . idNum . "`r`n"
			xPOS := button.getAttribute("av:Grid.Column")
			if StrLen(xPOS) < 2
				xPOS := "0" . xPOS
			yPOS := button.getAttribute("av:Grid.Row")
			if StrLen(yPOS) < 2
				yPOS := "0" . yPOS
			buttonArray["y" . yPOS . "x" . xPOS] := idNum
			; xmlDoc := loadXML(Clipboard)
			; txt := getButtonText(extractButtonText(xmlDoc, A_Index))
			; msgBox % txt . " - ID: " idNum " at position " xPOS . ", " . yPOS
		}
		temp := {}
		for key, val in buttonArray
			temp[val] ? temp[val].Insert(key) : temp[val] := [key]
		
		msg := ""
		for key, val in buttonArray
			msg := msg . val . "`r`n"
			
		; msgBox % "Result: `r`n" result
		; msgBox % "Sorted: `r`n" msg
		return msg
	} else {
		return b.getAttribute("OpsCommandNumber")
	}
}

;Extracts Node containing button text from @xmlDoc
;@index - index of item to retrieve (send negative index to retrieve entire array)
extractButtonText(xmlDoc, index=0)
{
	global TEXT_XPATH
	allTexts := xmlDoc.selectNodes(TEXT_XPATH)
	if index < 0
		return allTexts
	else
		return allTexts.item(index)
}

getButtonText(b)
{
	isArray := true
	try {
		b_length := b.length()
	} catch e {
		isArray := false
	}
	
	if (isArray) {
		result := ""
		for button in b
		{
			name := button.getAttribute("Text")
			xmlDoc := loadXML(Clipboard)
			idNum := getMenuID(extractButton(xmlDoc, A_Index - 1))
			result := result . name . "-" . idNum . "`r`n"
			;msgBox % name . " - " . idNum
		}
		return result
	} else {
		return b.getAttribute("Text")
	}
}

setButtonText(ByRef b, txt)
{
	x := ComObjCreate("MSXML2.DOMDocument.6.0")
	nodeText := x.createAttribute("Text")
	nodeText.value := txt
	b.setAttributeNode(nodeText)
}

setGenericAttribute(ByRef b, attribute, value)
{
	x := ComObjCreate("MSXML2.DOMDocument.6.0")
	nodeAttrib := x.createAttribute(attribute)
	nodeAttrib.value := value
	b.setAttributeNode(nodeAttrib)
}

loadXML(ByRef data)
{
  o := ComObjCreate("MSXML2.DOMDocument.6.0")
  o.async := false
  o.loadXML(data)
  return o
}



