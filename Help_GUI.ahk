ShowHotkeyShortcutGUI:
Gui, Add, GroupBox, x15 y22 w410 h150 , Universal Desktop Hotkey Shortcuts
Gui, Add, GroupBox, x15 y182 w410 h130 , EMC Hotkey Shortcuts
Gui, Add, Text, x35 y52 w40 h20 , Ctrl+F
Gui, Add, Text, x35 y82 w60 h20 , Shift+Enter
Gui, Add, Text, x35 y102 w60 h20 , Ctrl+Enter
Gui, Add, Text, x35 y122 w60 h20 , Alt+Enter
Gui, Add, Text, x145 y52 w100 h20 , Quick Search
Gui, Add, Text, x145 y82 w120 h20 , Search (Contains + SKU)
Gui, Add, Text, x145 y102 w120 h20 , Search (Begins with)
Gui, Add, Text, x145 y122 w140 h20 , Search (Contains + NO SKU)
Gui, Add, Text, x35 y212 w40 h20 , Ctrl+F
Gui, Add, Text, x145 y212 w100 h20 , Quick Search
Gui, Add, Text, x35 y232 w40 h20 , Alt+F
Gui, Add, Text, x145 y232 w180 h20 , Find (Default Ctrl+F Behavior)
Gui, Add, GroupBox, x15 y322 w410 h220 , Global Hotkeys
Gui, Add, Text, x35 y342 w40 h20 , Ctrl+C
Gui, Add, Text, x35 y382 w80 h20 , Ctrl+Win+C
Gui, Add, Text, x145 y342 w150 h30 , Copy (Trim Whitespace and Show length for Barcodes)
Gui, Add, Text, x145 y382 w180 h20 , Calculate Missing Barcode Digit
Gui, Add, Text, x35 y412 w80 h20 , Ctrl+Shift+C
Gui, Add, Text, x145 y412 w180 h20 , Begin UberClipboard Copy
Gui, Add, Text, x35 y442 w60 h20 , Ctrl+Insert
Gui, Add, Text, x145 y442 w180 h20 , UberClipboard - Paste Next Line
Gui, Add, Text, x35 y472 w80 h20 , Ctrl+Shift+Insert
Gui, Add, Text, x145 y472 w180 h30 , UberClipboard - Paste Next Line & Enter
Gui, Add, Text, x35 y262 w80 h20 , Alt+Shift+Insert
Gui, Add, Text, x145 y262 w180 h30 , Clear Barcodes & Search for next UberClipboard Entry
Gui, Add, Text, x35 y512 w80 h20 , Ctrl+Win+L
Gui, Add, Text, x145 y512 w180 h20 , Store Info Lookup
Gui, Add, GroupBox, x455 y22 w410 h210 , Remedy Hotkey Shortcuts
Gui, Add, Text, x465 y52 w80 h20 , Ctrl+Shift+F
Gui, Add, Text, x575 y52 w180 h20 , Mark Ticket Complete
Gui, Add, Text, x465 y82 w80 h20 , Ctrl+I
Gui, Add, Text, x575 y82 w180 h20 , Infogenesis - Change Adhoc
Gui, Add, Text, x465 y112 w80 h20 , Ctrl+Shift+I
Gui, Add, Text, x575 y112 w180 h20 , Infogenesis - Report Request
Gui, Add, Text, x465 y142 w80 h20 , Ctrl+E
Gui, Add, Text, x465 y172 w80 h20 , Ctrl+Shift+E
Gui, Add, Text, x575 y142 w180 h20 , Simphony - Change Adhoc
Gui, Add, Text, x575 y172 w180 h20 , Simphony - Report Request
Gui, Add, Text, x465 y202 w80 h20 , Ctrl+Shift+O
Gui, Add, Text, x575 y202 w180 h20 , Scrape Ticket to OneNote
Gui, Add, GroupBox, x455 y252 w410 h220 , Ticket Tracker
Gui, Add, Text, x465 y282 w80 h20 , Ctrl+Win+Home
Gui, Add, Text, x575 y282 w180 h20 , Show/Hide Ticket Tracker
Gui, Add, Text, x465 y312 w80 h20 , Ctrl+Win+Insert
Gui, Add, Text, x575 y312 w180 h20 , Lock/Unlock Ticket Tracker
Gui, Add, Text, x465 y342 w80 h30 , Ctrl+Win+Page Up
Gui, Add, Text, x575 y342 w180 h20 , Increase Ticket Count
Gui, Add, Text, x465 y382 w80 h30 , Ctrl+Win+Page Down
Gui, Add, Text, x575 y382 w180 h20 , Decrease Ticket Count
Gui, Add, Text, x505 y432 w310 h30 , Note: Marking a Ticket Complete with Ctrl+Shift+F in Remedy will automatically increase ticket count.
Gui, Add, Text, x476 y483 w120 h20 , Uber Clipboard Hotkeys
; Generated using SmartGUI Creator 4.0
Gui, Show, x130 y90 h556 w940, Compass AHK Tweaks Shortcuts
return

GuiClose:
ExitApp