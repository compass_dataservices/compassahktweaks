import xml.etree.ElementTree as ET
import os

NS = {'default': 'clr-namespace:EMC.Application.General.Controls.MicrosGridEditor;assembly=EMC', 'mouic': 'clr-namespace:Micros.OpsUI.Controls;assembly=OpsUI'}
XML_DOC = 'EMC_Multiple_Button_Test.xml'

def get_button_data(root):
    for Button in root.findall('mouic:Button', NS):
        for k,v in Button.attrib.items():
            print(k, '=', v)
            
    # for b in root.findall('mouic:Button', NS):
        # for tem in Button.findall('mouic:TraNSlatedEntryManager.TraNSlatedEntryList', NS):
            # for tec in tem.findall('mouic:TraNSlatedEntryCollection', NS):
                # for te in tec.findall('mouic:TraNSlatedEntry', NS):
                    # for k,v in te.attrib.items():
                        # print(k, '=', v)
                       
                       
def main():
    tree = ET.parse(XML_DOC)
    root = tree.getroot()
    get_button_data(root)
    
    
if __name__ == "__main__":
    main()