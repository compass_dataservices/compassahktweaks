#NoTrayIcon
#SingleInstance Force

	if 0 < 7
	{
		MsgBox, Not enough parameters!`n%0%
		ExitApp
	} else {
		Customer = %1%
		Enterprise = %2%
		Division = %3%
		Store = %4%
		System = %5%
		Pos_Solution = %6%
		Unit = %7%
		if (Pos_Solution == "IG") {
			Gui, StoreInfoGUI: Add, Text, x15 y32 w50 h20 , Customer
			Gui, StoreInfoGUI: Add, Text, x15 y72 w50 h20 , Enterprise
			Gui, StoreInfoGUI: Add, Text, x15 y112 w50 h20 , Division
			Gui, StoreInfoGUI: Add, Text, x15 y152 w50 h20 , Store
			Gui, StoreInfoGUI: Add, Text, x15 y192 w50 h20 , System
			Gui, StoreInfoGUI: Add, Edit, x75 y32 w160 h20 ReadOnly vCustomerEdit, % Customer
			Gui, StoreInfoGUI: Add, Edit, x75 y72 w160 h20 ReadOnly vEnterpriseEdit, % Enterprise
			Gui, StoreInfoGUI: Add, Edit, x75 y112 w160 h20 ReadOnly vDivisionEdit, % Division
			Gui, StoreInfoGUI: Add, Edit, x75 y152 w160 h20 ReadOnly vStoreEdit, % Store
			Gui, StoreInfoGUI: Add, Edit, x75 y192 w160 h20 ReadOnly vSystemEdit, % System
			Gui, StoreInfoGUI: Add, Button, x245 y32 w60 h20 gCopyDetails, Copy &1
			Gui, StoreInfoGUI: Add, Button, x245 y72 w60 h20 gCopyDetails, Copy &2
			Gui, StoreInfoGUI: Add, Button, x245 y112 w60 h20 gCopyDetails, Copy &3
			Gui, StoreInfoGUI: Add, Button, x245 y152 w60 h20 gCopyDetails, Copy &4
			Gui, StoreInfoGUI: Add, Button, x245 y192 w60 h20 gCopyDetails, Copy &5
			Gui, StoreInfoGUI: Add, Button, x108 y222 w100 h30 gCopyAll, Copy All
			Gui, StoreInfoGUI: Add, Button, Default x108 y262 w100 h30 gLaunchApp, Login
			Gui, StoreInfoGUI:+AlwaysOnTop
			Gui, StoreInfoGUI: Show, x322 y199 h307 w317, Store Info
		} else {
			Gui, StoreInfoGUI: Add, Text, x15 y32 w50 h20 , Store
			Gui, StoreInfoGUI: Add, Edit, x75 y32 w225 h20 ReadOnly vStoreEdit, % Store
			Gui, StoreInfoGUI: Add, Button, Default x108 y60 w100 h30 gLaunchApp, Login
			Gui, StoreInfoGUI: Show, x322 y199 h100 w317, Store Info
		}
		; Generated using SmartGUI Creator 4.0
		
	}
Return

CopyDetails:
	StringRight, eVar, A_GuiControl, 1
	if (eVar = 1)
	{
		Clipboard := Customer
	}
	else if (eVar = 2)
	{
		Clipboard := Enterprise
	}
	else if (eVar = 3)
	{
		Clipboard := Division
	}
	else if (eVar = 4)
	{
		Clipboard := Store
	}
	else if (eVar = 5)
	{
		Clipboard := System
	}
Return

CopyAll:
	Clip =
		( LTrim
		Store: %Store%
		System: %System%
		Enterprise: %Enterprise%
		Customer: %Customer%
		Division: %Division%
		)
	Clipboard := Clip
	ClipWait,2
	MsgBox,,% "CompassAHKTweaks", % "Copied", 5
ExitApp

CopyAllSpecial:
	Clip =
		(LTrim
		System - %System%
		Enterprise - %Enterprise%
		Unit - %Store%
		)
	Clipboard := Clip
	ClipWait,2
	MsgBox,,% "CompassAHKTweaks", % "Copied Special", 5
ExitApp

#IfWinActive Store Info

^a::
	GoSub,CopyAll
Return

^+a::
	GoSub,CopyAllSpecial
Return

LaunchApp:
	if (Pos_Solution == "IG")
		GoSub, GoUD
	else
		GoSub, GoEMC
ExitApp

GoEMC:
	IfWinExist, ahk_exe EMC.exe
	{
		WinActivate
		ControlFocus, WindowsForms10.EDIT.app.0.378734a1
		Send {HOME}
		Send +{END}
		Send {BS}
		Sleep, 300
		Control, EditPaste, %Unit%, WindowsForms10.EDIT.app.0.378734a1, EMC
	} else {
		msgBox % "Open EMC then try login again."
	}
return

GoUD:
	IfNotExist, %A_AppData%/CompassAHKTweaks
	{
		FileCreateDir, %A_AppData%/CompassAHKTweaks
	}
	SetWorkingDir, %A_AppData%/CompassAHKTweaks

	IfNotExist, %A_WorkingDir%\CompassTweaks.ini
	{
		FileSelectFile, UDEXE, 3,, Universal Desktop Location
		If ErrorLevel {
			msgBox, 48, Error, You must specify Universal Desktop Location to continue.
			ExitApp
		}
		IniWrite, %UDEXE%, %A_WorkingDir%\CompassTweaks.ini, Universal Desktop, Location
	} else {
		IniRead, UDEXE, %A_WorkingDir%\CompassTweaks.ini, Universal Desktop, Location
	}
	
	Process, Exist, UniversalDesktop.exe
		if (errorlevel) {
			Process, Close, %errorlevel%
			Process, WaitClose, %errorlevel%, 5
			if (errorlevel) {
				msgBox Unable to kill UD
				return
			}
		}
		Run, %UDEXE%
return

Esc::
GuiEscape:
GUIClose:
ExitApp