;;;;;;;;;;;;;;;;;;;;;;;
;   Global Hotkeys    ;
;;;;;;;;;;;;;;;;;;;;;;;

breakUncheker := false

;reload script
^!z::
	if (DebuggingEnabled && GlobalHotkeysEnabled)
		GoSub,Reloader
return

;testing infos
^!#k::
	if (GlobalHotkeysEnabled && DebuggingEnabled)
	msgBox, % "AHK Version: " . A_AhkVersion . "`nUnicode: " . A_IsUnicode . "`nPointer Size: " . A_PtrSize . "`nUser: " . A_UserName . "`nSecreen Width: " . A_ScreenWidth
return

^F12::
	TrayTip,,Breaking loop
	breakUncheker := true
return

^4::
	Send {DEL 5}
	Sleep, 300
	Send ^{Down 4}
	Send {Left}
return

^+d::
	if (GlobalHotkeysEnabled && DebuggingEnabled) {
		Run, %A_ScriptDir%\DBAExampleOOP.ahk
	}
return

;^#L
^#l:: 
GetStoreInfo:
	if (GlobalHotkeysEnabled) {
		InputBox, UnitNumber, System Selection, Please enter a unit number:,,220,150,,,,,%lastUnitNumber%
		lastUnitNumber := UnitNumber
		if (errorlevel) {
			return
		} else {
			if (DebuggingEnabled)
				data := UnitSearch(UnitNumber)
			else
				data := UnitSearch(UnitNumber)
				
			if (data != "acket") {
				ExtractUnitInfo(data)
			} else {
				msgBox,% "Unable to locate unit"
				return
			}
			if (DebuggingEnabled)
				Run, %A_ScriptDir%\StoreInfoGUI.ahk "%unitCustomer%" "%unitEnterprise%" "%unitDivision%" "%unitName%" "%unitSystem%" "%unitPOS%" "%UnitNumber%"
			else
				Run, %A_ScriptDir%\StoreInfoGUI.exe "%unitCustomer%" "%unitEnterprise%" "%unitDivision%" "%unitName%" "%unitSystem%" "%unitPOS%" "%UnitNumber%"
		}
	}
Return

^#PgUp::
IncreaseTicketCount:
	 if (GlobalHotkeysEnabled) {
		FormatTime, iniDate, %A_Now%, yyMMdd
		IniRead, ticketCount, %ticketCountIni%, Ticket Count, %iniDate%, 0
		ticketCount++
		TrayTip,,% "[+] Ticket Count: " . ticketCount
		GuiControl, TicketCountGUI:, TicketCount, %TicketCount%
		IniWrite, %ticketCount%, %ticketCountIni%, Ticket Count, %iniDate%
	}
return

^#PgDn::
DecreaseTicketCount:
	if (GlobalHotkeysEnabled) {
		FormatTime, iniDate, %A_Now%, yyMMdd
		IniRead, ticketCount, %ticketCountIni%, Ticket Count, %iniDate%, 0
		ticketCount--
		TrayTip,,% "[-] Ticket Count: " . ticketCount
		GuiControl, TicketCountGUI:, TicketCount, %TicketCount%
		IniWrite, %ticketCount%, %ticketCountIni%, Ticket Count, %iniDate%
	}
return

^#Home::
DisplayTicketCount:
	if (GlobalHotkeysEnabled) {
		GoSub,UpdateTicketCount
		GoSub,ToggleTicketCountGUI
	}
return

^#Ins::
	if (GlobalHotkeysEnabled)
		GoSub,ToggleLockTicketCountGUI
return

Reloader:
	TrayTip, AutoHotkey, Script Reloading, 10, 1
	Sleep 1000 
	Reload
	Sleep 1000 
	;If successful, the reload will close this instance during the Sleep, so the line below will never be reached.
	MsgBox, Reload Failed!
return

;QuickNote Everywhere
$^+m::
QuickNote:
	titleMatchStore := A_TitleMatchMode
	SetTitleMatchMode, 2
	IfWinActive, OneNote
	{
		TrayTip, CompassAHKTweaks, Launching QuickNote
		Send ^+m
	} else {
		IfWinExist, OneNote
		{
			WinActivate, OneNote
			If (ErrorLevel) {
				msgBox % "Unable to Activate OneNote"
				return
			}
			Send ^+m
		} else {
			IfMsgBox, Yes
			{
				if A_Is64bitOS
					Run, ONENOTE.EXE, C:\Program Files (x86)\Microsoft Office\Office15\
				else
					Run, ONENOTE.EXE, C:\Program Files\Microsoft Office\Office15\
			}
		}
	}
	Sleep, 1000
	SetTitleMatchMode, %titleMatchStore%
	MouseGetPos, mX, mY
	WinMove, Untitled,, mX, mY
return

;testing for date parser
^!y::
	If (DebuggingEnabled) {
		InputBox, monthString, Date, Please enter a month:
		InputBox, dayString, Date, Please enter a day:
		InputBox, yearString, Date, Please enter a year:
		if (monthString != "") and (dayString != "") and (yearString != "") {
			weekday := Date2Day(monthString, dayString, yearString)
			msgBox, % "Day of the week is " . weekday
		}
	}
return

;Always-On-Top
^#t::
	if (GlobalHotkeysEnabled) {
		MouseGetPos,,,MouseOverWindow
		WinSet, AlwaysOnTop, Toggle, ahk_id %MouseOverWindow%
		WinGet, ExStyle, ExStyle, ahk_id %MouseOverWindow%
		alwaysTop := False
		if (ExStyle & 0x8) {
			ToolTip, Always-On-Top Activated
			alwaysTop := True
		} else {
			ToolTip, Always-On-Top Deactivated
		}
		SetTimer, RemoveToolTip, 2000
	}
return

Pause::Pause

;Copy List for rapid paste with uberClipboard
^+c::
uberClipCopy:
	if (GlobalHotkeysEnabled) {
		Keywait, Control
		Keywait, Shift
		oldClip := ClipboardAll
		Clipboard=
		Send ^c
		Clipwait, 2
		sendToUberClipboard()
		Sleep,300 ;necessary to diminish concurrency issues
		if (oldClip) {
			Clipboard=
			Clipboard = %oldClip%
			ClipWait,1
		}
	}
return

sendToUberClipboard:
	sendToUberClipboard()
return

sendToUberClipboard()
{
	global DebuggingEnabled
	if (DebuggingEnabled == true)
	{
		TrayTip, CompassAHKTweaks, sending to uberClip AHK
		Run, %A_ScriptDir%\uberClipboard.ahk c
	} else {
		TrayTip, CompassAHKTweaks, sending to uberClip EXE
		Run, %A_ScriptDir%\uberClipboard.exe c
	}
}

;identify and trim barcodes when copying
$~^c::
	if (BarcodeCopyToolsEnabled) {
		try {
			ClipWait
			Sleep 300
			if (RegExMatch(Clipboard, "[\d\s]*\d+[\d\s]*", upcvar) && !RegExMatch(clipboard, "[^\d^\W]+")
				and !RegExMatch(clipboard, "[\.\-\(\)]")) {
				clipboard=%upcvar%
				StringReplace, clipboard, clipboard, %A_Space%,,All
				StringReplace, clipboard, clipboard, -,,All
				Sleep 500
				StringLen, length, clipboard
				ToolTip, Length: %length%
			}
			sleep, 1000
			ToolTip
		} catch e {
			TrayTip, CompassAHKTweaks, Unable to parse clipboard
		}
	}
return

;UPC Checksum testing
^#c::
	if (GlobalHotkeysEnabled) {
		if RegExMatch(Clipboard, "[\d\s]*\d+[\d\s]*") {
			StringLen, length, Clipboard
			if (length = 11) {
				result := CorrectBarcode(clipboard)
				clipboard=%result%
				ToolTip, Corrected UPC: %result%
			} else {
				ToolTip, UPC length must be 11 digits.
			}
		} else {
			ToolTip, No valid UPC found.
		}
		Sleep 2000
		ToolTip
	}
return

;barcode JSON fetch stub
^#b::
	if (DebuggingEnabled && GlobalHotkeysEnabled) {
		RegExMatch(clipboard, "0*\d+")
		if (ErrorLevel = 0)
		{
			barcode=%clipboard%
			TrayTip, AutoHotkey, Barcode Found, 10, 1
		} else {
			barcode="0"
			TrayTip, AutoHotkey, Zero'd Barcode, 10, 1
		}
		Gosub,JSONTest
	}
return

;trap inadvertent window focus loss ctrl+f shenanigans
^f::
	if (GlobalHotkeysEnabled) {
		MouseGetPos,,,MouseOverWindow
		WinGet, MouseOverProcess, ProcessName, ahk_id %MouseOverWindow%
		if (MouseOverProcess = "EMC.exe") {
			WinActivate, ahk_class WindowsForms10.Window.8.app.0.378734a
		} else if (MouseOverProcess = "UniversalDesktop.exe") {
			WinActivate, ahk_class WindowsForms10.Window.8.app.0.33c0d9d
		} else {
			Send ^f
		}
	} else {
		Send ^f
	}
return

;Bullet-Proof UD start-up
^#u::
	if (GlobalHotkeysEnabled) {
		GoSub, BulletProofUDLaunch
	}
return

WaitForCursor:
	Loop
	{
		IfEqual, A_Cursor, Wait, break ;wait for cursor to go to wait status
		Sleep, 10
		if (A_Index > 500) {
			TrayTip, CompassAHKTweaks, Search failed
			return
		}
	}
	Loop
	{
		IfNotEqual, A_Cursor, Wait, break	;so long as we are still getting a non regular cursor, wait
		Sleep, 10
		if (A_Index > 3000) {
			msgBox Search Timed Out!
			break
		}
	}
	Sleep, 300
return

;WIP Icon OneNote	
^+1::
	if (GlobalHotkeysEnabled) {
		oldClip := ClipboardAll
		Sleep, 300
		if (HasCustomChar()) {
			Send {U+231B}
		} else {
			Send {U+231B} %Clipboard%
		}
		Clipboard := oldClip
		ClipWait, 1
	}
return	
	
^+0::
Incredipaste:
	if (GlobalHotkeysEnabled) {
		acket := Clipboard
		Sleep 100
		acket++
		Sleep 100
		Clipboard := acket
		Clipwait,1
		ControlGetFocus, c, A
		if (c = EMC_BARCODES_LIST or c = EMC_BARCODES_MASTER_EDIT) {
			Send %acket%
			Send {Down}
		} else {
			Send ^v
		}
        Sleep 100
	}	
return

;empty checkbox
^+2::
OneNoteTaskBox:
	if (DebuggingEnabled && GlobalHotkeysEnabled) {
		oldClip := ClipboardAll
		Sleep, 300
		Send ^{Home}
		Send ^{Up}
		Send ^{Left 2}
		if (HasCustomChar()) {
			Send {U+2610}
		} else {
			Send {U+2610} %Clipboard%
		}
		Clipboard := oldClip
		Clipwait, 1
	}
return

;checked box
^+3::
OneNoteTaskComplete:
	if (DebuggingEnabled && GlobalHotkeysEnabled) {
		oldClip := ClipboardAll
		Sleep, 300
		Send ^{Home}
		Send ^{Up}
		Send ^{Left 2}
		if (HasCustomChar()) {
			Send {U+2611}
		} else {
			Send {U+2611} %Clipboard%
		}
		Clipboard := oldClip
		ClipWait, 1
	}
return

;telephone
^+4::
	if (DebuggingEnabled && GlobalHotkeysEnabled) {
		OldClip := ClipboardAll
		Sleep, 300
		Send ^{Home}
		Send ^{Up}
		Send ^{Left 2}
		if (HasCustomChar()) {
			Send {U+260E}
		} else {
			Send {U+260E} %Clipboard%
		}
		Clipboard := oldClip
		ClipWait, 1
	}
return

;U+214D = Agilysys?
^!a::
	if (DebuggingEnabled && GlobalHotkeysEnabled) {
		Send {U+214D}
	}
return

;U+260E BLACK TELEPHONE
^!c::
	if (DebuggingEnabled && GlobalHotkeysEnabled) {
		Send {U+260E}
	}
return

;U+2318 PLACE OF INTEREST (MICROS)
^!m::
	if (DebuggingEnabled && GlobalHotkeysEnabled) {
		Send {U+2318}
	}
return

;U+24B6	CIRCLED LATIN CAPITAL LETTER A
;U+2622	RADIOACTIVE SIGN
;U+2630	TRIGRAM FOR HEAVEN


;testing Unicode chars
^+-::
	if (DebuggingEnabled && GlobalHotkeysEnabled) {
		Send {U+2630}
	}
return

;enhanced barcode paste with uberClipboard
$!+Ins::
	IfWinNotActive, ahk_exe EMC.EXE
	{
		WinGetTitle, lastWindow, A
		Sleep, 300
		WinActivate, EMC
		GoSub, EMCUberBarcodeSearch
		Sleep, 500
		WinActivate, %lastWindow%
	}
return
		

HasCustomChar()
{
	Sleep, 300
	Clipboard=
	Keywait, Ctrl
	Send {Home}
	Send +{Right}
	Sleep, 100
	Send ^c
	ClipWait, 2
	Transform, asciiCode, Asc, %Clipboard%
	if (asciiCode < 200) {
		return false
	} else {
		return true
	}
	Sleep, 300
}

;convenience feature for typing menu ids in the 500M range
^+5::
	if (GlobalHotkeysEnabled)
		Send 00000
return

^+6::
	if (GlobalHotkeysEnabled)
		Send 000000
return

^+7::
	if (GlobalHotkeysEnabled)
		Send 0000000
return

NumpadIns::Ctrl

;converts NumpadAdd to backspace when Numlock is off
$NumpadAdd::
	if (GlobalHotkeysEnabled && DebuggingEnabled) {
		if (!GetKeyState("Numlock", "T")) {
			Send {BS}
		} else {
			Send {NumpadAdd}
		}
	} else {
		Send {NumpadAdd}
	}
return

;Play/Pause fix
~SC122::
	Sleep 100
	IfWinExist, ahk_class #32770 
	{
		ControlFocus, OK, ahk_class #32770
		ControlSend, OK, {SPACE}, ahk_class #32770
	}
return

;Close TeamViewer Popups
TeamViewerPopupWatcher:
	IfWinExist, Sponsored session ahk_exe TeamViewer.exe
	{
		ControlSend, OK, {Space}
	}
return

;Paste ticket details to OneNote
^+v::
PasteTicketToOnenote:
	titleMatchStore := A_TitleMatchMode
	SetTitleMatchMode, 2
	IfWinActive, OneNote
	{
		if (CheckOneNoteVariables(UnitNum, requesterName, requesterPhone, requesterEmail) = 1)
			return
		
		data := UnitSearch(UnitNum)
		if (data != "acket") {
			ExtractUnitInfo(data)
		} else {
			unitSystem := "EMC?"
		}
		
		IniRead, noteFormat, CompassTweaks.ini, One Note, Format, "!sys!`n!task!`n!name!`n!email!"
		If (noteFormat != "nosystem") {
			firstLine := " " . unitNum . " <" . unitSystem . ">"
		} else {
			firstLine := unitNum
		}
		Clipboard=
		Clipboard := firstLine
		ClipWait, 1
		Send ^v{Enter}{Up}
		Sleep 300
		datas := BuildOneNotePaste(requesterName, requesterPhone, requesterEmail, taskID, ticketContents)
		GoSub,ClearTicketVars
		Clipboard=
		Clipboard := datas
		ClipWait, 1
		Send ^v
	}
	SetTitleMatchMode, %titleMatchStore%
return

BuildOneNotePaste(name, phone, email, task, contents)
{
	if (name = "No Name") 
		name =
	else
		name := name . "`n"
		
	if (phone = "(000) 000-0000" or phone = "No Phone")
		phone =
	else
		phone := phone . "`n"
		
	if (email = "No Email")
		email =
	else
		email := email . "`n"
		
	pasteData := task . "`n" . name . phone . email . "`n`n" . contents
	return %pasteData%
}

CheckOneNoteVariables(num, name, phone, email)
{
	if (num = "" || name = "" || phone = "" || email = "") {
		msgBox, One or more pieces of data missing.  Please try again.
		return 1
	}
	return 0
}

IsSpotifyMusicPlaying()
{
	IfWinExist, ahk_class SpotifyMainWindow
	{
		IfWinNotExist, Spotify
		{
			return true
		} else {
			return false
		}
	} else {
		ErrorLevel := 1
		return false
	}
}

:*:ftsk::FTSK-00000
:*:#AG#::1-800-327-7088
:*:#MC#::1-800-937-2211
:*:#fp#::(>{U+10DA})



#IfWinActive ahk_exe OUTLOOK.EXE
^f::
	Send ^e
Return

:*:thbr::
	GoSub,thanks_reload
return

:*:thbc::
	GoSub,thanks_completed
return

:*:tfsd::
	GoSub,thanks
return

#IfWinActive ahk_class LyncConversationWindowClass
!s::
	Send {Enter}
return

#IfWinActive FSS:Console
^#w::
	WinMove, FSS:Console,, 340, 123, 1200, 780
return

#IfWinActive ahk_group RemedyGroup
thanks_reload:
:*:thbr::
	Send This has been completed.  You should see the changes after you perform a reload config on the terminal(s).  Let us know if you need anything else.
	Send {Enter 2}
	GoSub, thanks
return

thanks_completed:
:*:thbc::
	Send This has been completed.  Let us know if you need anything else.
	Send {Enter 2}
thanks:
:*:tfsd::
	Send Thanks,{enter}
	IfWinActive, ahk_exe outlook.exe
	{
		return
	} else {
		Send FSS Data Services
		Send {TAB 3}
		IfWinNotActive, ahk_exe aruser.exe
		{
			Sleep 100
			Send {TAB}
		}
	}
return

#IfWinActive ahk_exe chrome.exe

~^e::
	If (GlobalHotkeysEnabled) {
		sendMailActivationAttempts := 0
		SetTimer, ActivateSendMailWin, 100
	}
return

ActivateSendMailWin:
	If (sendMailActivationAttempts > 30) {
		MsgBox, Failed to locate FSS Send Mail Window
		SetTimer, ActivateSendMailWin, Off
	}
	IfWinExist, FSS Send Mail
	{
		WinActivate, FSS Send Mail
		SetTimer, ActivateSendMailWin, Off
	} else {
		sendMailActivationAttempts++
	}
return

#IfWinExist ahk_exe slack.exe

#s::
	If (SlackHotkeyEnabled) {
		WinGet, slackMinStatus, MinMax, ahk_exe slack.exe
		If (slackMinStatus == -1) {
			WinRestore, ahk_exe slack.exe
		} else {
			WinMinimize, ahk_exe slack.exe
		}
	} else {
		Send #s
	}
return
